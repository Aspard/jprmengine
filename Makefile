AGRUM_INCLUDE_PATH=-I./lib/agrum/include
AGRUM_LIB_PATH=./lib/agrum/lib
AGRUM=../aGrUM/build/release
LIBS= -lc -lstdc++ -o build/libPRMengine.so -lagrum
COMPILATION_FLAGS=-g
GPP=g++-9

#linux
#JAVA_INCLUDE=-I/usr/lib/jdk1.8.0_121/include -I/usr/lib/jdk1.8.0_121/include/linux
#Mac
#JAVA_INCLUDE=-I/Applications/Xcode.sopp/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.15.sdk/System/Library/Frameworks/JavaVM.framework/Versions/A/Headers

JAVA_INCLUDE=-I/usr/lib/jvm/java-8-openjdk-amd64/include -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux

JAVA_PATH_ENGINE=agrum/prmEngine
JAVA_DOMAIN_ENGINE:= $(subst /,.,$(JAVA_PATH_ENGINE))

PRIME_JAR_PATH=../output/jar
PRIME_LIB_PATH=../output/lib

all: getlib test deploy

getlib:
	@ls ../aGrUM/build/release
	@echo "Getting aGrUM files..."
	@mkdir ./lib/agrum
	@cp -r ${AGRUM}/include ./lib/agrum
	@cp -r ${AGRUM}/lib ./lib/agrum

deploy:
	@cp build/PRMengine.jar ${PRIME_JAR_PATH}
	@cp build/libPRMengine.so ${PRIME_LIB_PATH}
	@cp lib/agrum/lib/libagrum*.so* ${PRIME_LIB_PATH}

src/IncPRMengine_wrap.cxx java/org/${JAVA_PATH_ENGINE}/IncPRMengine.java: PRMengine.i
	@echo "Generating IncPRMengine wrapper ..."
	@mkdir -p java/org/$(JAVA_PATH_ENGINE)
	@swig -c++ -java -package org.${JAVA_DOMAIN_ENGINE} -outdir java/org/${JAVA_PATH_ENGINE} -o src/IncPRMengine_wrap.cxx PRMengine.i

obj/IncPRMengine_wrap.o: src/IncPRMengine_wrap.cxx java/org/${JAVA_PATH_ENGINE}/IncPRMengine.java
	@echo "Compiling IncPRMengine wrapper ..."
	@mkdir -p obj
	@${GPP} -std=c++14 -fPIC -c src/IncPRMengine_wrap.cxx ${JAVA_INCLUDE} ${AGRUM_INCLUDE_PATH} -g -o3 -o obj/IncPRMengine_wrap.o

obj/IncPRMengine.o: src/IncPRMengine.cpp src/IncPRMengine.h src/IncSystem.h src/IncSystem.cpp src/PRMexplorer.h src/PRMexplorer.cpp
	@echo "Compiling IncPRMengine ..."
	@mkdir -p obj
	@${GPP} -std=c++14 -fPIC -c src/IncPRMengine.cpp ${AGRUM_INCLUDE_PATH} -g -o3 -o obj/IncPRMengine.o

obj/IncSystem.o:
	@echo "Compiling IncSystem ..."
	@mkdir -p obj
	@${GPP} -std=c++14 -fPIC -c src/IncSystem.cpp ${AGRUM_INCLUDE_PATH} -g -o3 -o obj/IncSystem.o

obj/PRMexplorer.o:
	@echo "Compiling PRMexplorer ..."
	@mkdir -p obj
	@${GPP} -std=c++14 -fPIC -c src/PRMexplorer.cpp ${AGRUM_INCLUDE_PATH} -g -o3 -o obj/PRMexplorer.o

build/libPRMengine.so: obj/IncPRMengine_wrap.o obj/IncPRMengine.o obj/IncSystem.o obj/PRMexplorer.o
	@echo "Linking shared library..."
	@mkdir -p build
	@${GPP} -std=c++14 -shared obj/IncPRMengine_wrap.o obj/IncPRMengine.o obj/IncSystem.o obj/PRMexplorer.o ${LIBS} -L${AGRUM_LIB_PATH}

test: test/PRMengine.jar test/TestEngine.class
	@echo "Running test...\n\n"
	@cd test;LD_LIBRARY_PATH=.:${AGRUM_LIB_PATH}:${LD_LIBRARY_PATH} LC_ALL=C java -cp PRMengine.jar:. TestEngine

test/TestEngine.class:test/TestEngine.java
	@echo "Compiling test...\n\n"
	@cd test;LD_LIBRARY_PATH=.:${AGRUM_LIB_PATH}:${LD_LIBRARY_PATH} LC_ALL=C javac -cp PRMengine.jar:. TestEngine.java

test/PRMengine.jar:build/libPRMengine.so build/PRMengine.jar
	@echo "Building test ..."	
	@cp build/PRMengine.jar test/.
	@cp ${AGRUM_LIB_PATH}/libagrum*.so* test/.
	@cp build/libPRMengine.so test/.
	@cd test;javac -cp PRMengine.jar *.java

build/PRMengine.jar:  java/org/${JAVA_PATH_ENGINE}/PRMengine.java
	@echo "Building jar library ..."
	@mkdir -p obj/org/$(JAVA_PATH_ENGINE)
	@javac -g -Xlint:deprecation -d obj java/org/${JAVA_PATH_ENGINE}/*.java
	@cd obj;jar -cf ../build/PRMengine.jar org

clean:
	@echo "Cleaning all ..."
	@rm -rf build
	@rm -rf obj
	@rm -rf java
	@rm -f core* hs_err*
	@rm -f test/*.class
	@rm -f test/*.jar
	@rm -f test/*.so
	@rm -f src/*_wrap.*
	@rm -rf lib/agrum

.PHONY: all clean test