## -*- coding: utf-8 -*-
#"""
#Created on Sun Nov 27 00:36:37 2016
#
#@author: Hamza
#"""

import matplotlib.pyplot as plt
import numpy as np
import glob   
import fileinput 
path='../resources/incremental_log/'
figurepath='C:/Users/Hamza/Dropbox/PhD/Documentation/Urbs/figures/ijtiTimes/'
mark=['*', '+', '.', '<', '>', '^', 'd', 'h', 'o', 'p', 's', 'v', 'x']
#percentages=[.03,.06,.11,.17,.22,.28,.33,.39]


for bn_log in  glob.glob(path+'alarm-0.1-0.1.txt'):
    print(bn_log)
    dt=bn_log.rfind('.')
    slsh=bn_log.rfind('/')+1
#    for line in fileinput.input(bn_log, inplace=1):
#        if fileinput.isfirstline():
#            line=line
#        elif line.startswith("19"):
#            line=""
#        print(line, end="")
    a=np.loadtxt(bn_log,delimiter=",")
    nb_iter_per_percnt=int(a[0,0]+1)
    percentages=a[0,1:np.where(a[0,1:]==0)[0][0]+1]
    a=a[1:,:]
    nb_percnt=int(len(a)/nb_iter_per_percnt)
    for perc in percentages:
        lp_mean_per_percnt,inc_mean_per_percnt,lp_std_per_percnt,inc_std_per_percnt=[],[],[],[]
        for i in range(nb_percnt):
            #use a[i*nb_iter_per_percnt,0]==perc to have evidence in x and 1 for targets
            if round(a[i*nb_iter_per_percnt,0],2)==round(perc,2):
#             print('{}__{}---{}'.format(i,a[i*nb_iter_per_percnt,0],a[i*nb_iter_per_percnt,1])   )
             lp_mean_per_percnt.append(np.mean(np.mean(a[i*nb_iter_per_percnt+1:(i+1)*nb_iter_per_percnt,1::2],axis=1)))
             lp_std_per_percnt.append(np.std(a[i*nb_iter_per_percnt+1:(i+1)*nb_iter_per_percnt,1::2]))
                
             inc_mean_per_percnt.append(np.mean(np.mean(a[i*nb_iter_per_percnt+1:(i+1)*nb_iter_per_percnt,0::2],axis=1)))
             inc_std_per_percnt.append(np.std(a[i*nb_iter_per_percnt+1:(i+1)*nb_iter_per_percnt,0::2]))
# #      pLot times vs targets (evidence)  percentages
#        plt.errorbar(percentages,lp_mean_per_percnt,lp_std_per_percnt,
#                    capsize=3, capthick=1.2,
#                    label="Lazy Propagation")    
#        plt.errorbar(percentages,inc_mean_per_percnt,inc_std_per_percnt,
#                     fmt='ro-',capsize=3, capthick=1.2,
#                     label="Incremental Inference")        
#        plt.title("{}-{}".format(bn_log[slsh+7:dt],perc))
#        plt.xlabel("evidences %")
#        ## uncomment next line if a[i*nb_iter_per_percnt,1]==perc
##        plt.xlabel("targets %")
#        plt.ylabel("time gain %")
#        plt.legend(loc=4)
#        plt.savefig(figurepath+"{}-targ-{}".format(bn_log[slsh:dt],perc)+".eps", format='eps', dpi=1000)
#        ## uncomment next line if a[i*nb_iter_per_percnt,1]==perc
##        plt.savefig(figurepath+"{}-ev-{}".format(bn_log[slsh:dt],perc)+".eps", format='eps', dpi=1000)
#        plt.show()
      

###        plot the gain (gain_evidence=f(target))
        plt.plot(percentages,
                 np.ones(len(inc_mean_per_percnt))-np.array(inc_mean_per_percnt)/np.array(lp_mean_per_percnt),
                 '-',label=perc)
        plt.grid('on')
        plt.xlabel('evidences %',fontsize ='x-large')
#        plt.xlabel('targets %',fontsize ='x-large')
        plt.ylabel('time gain % ',fontsize ='x-large') 
    plt.grid('on')
    plt.legend(ncol=3,fancybox=True)
    plt.title(bn_log[slsh:dt])
    plt.savefig(figurepath+"{}-targ-time-gains.eps".format(bn_log[slsh:dt]), format='eps', dpi=1000)
#    plt.savefig(figurepath+"{}-ev-time-gains.eps".format(bn_log[slsh:dt]), format='eps', dpi=1000)
    plt.show()