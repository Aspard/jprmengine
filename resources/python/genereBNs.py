from __future__ import division
import matplotlib
matplotlib.use("Qt4Agg")

import pyAgrum as gum


import random
import datetime
savepath='C:/Dev/aGrUM/urbs/ressources/IJTI_gen/veryLarge100-1250/'

def generateBN(savepath,size,it1):
  generator=gum.BNGenerator()
  ts=datetime.datetime.now().strftime("%Y%m%d%H%M")
  for s in size:
    for i in range(it1):
      nbArcs=random.choice(range(s-1,int(s-1+s/3)))
      filename="bn{0}-{1}-{2}.bif".format(ts,s,i)
      print(filename)
      bn=generator.generate(s,nbArcs,2)
      gum.saveBN(bn,savepath+'/'+filename) 

generateBN(savepath,[900,1050,1200,1250],3)