
from __future__ import division
import matplotlib
matplotlib.use("Qt4Agg")


import pydotplus as dot
import pyAgrum as gum
from notebook import showGraph

import glob
import numpy as np


import random
import datetime
import IPython.display
from IPython.core.pylabtools import print_figure
from IPython.core.display import Image,display_png,display_html



resourcespath='C:/Dev/aGrUM/urbs/ressources/'
logpath='C:/Dev/aGrUM/urbs/ressources/log_results/new_logs/rand/'
  


def showBN ( bn, bgcolor="#6b85d1", fgcolor="#FFFFFF",size="6" ):
    graph = dot.Dot( "bn", graph_type='digraph',size=size)
    node_names=bn.names()

    fmt=lambda name :  "{0}-{1}".format(bn.idFromName(name),name)
    
    # création des noeuds du réseau
    for name in node_names:
        new_node = dot.Node( fmt(name),
                               style="filled",
                               fillcolor=bgcolor,
                               fontcolor=fgcolor )
        graph.add_node( new_node )

    # création des arcs
    for name in node_names:
        for par in bn.parents(bn.idFromName(name)):
            new_edge = dot.Edge ( fmt(bn.variable(par).name()), fmt(name))
            graph.add_edge ( new_edge )

    # affichage
    png_graph=graph.create_png()
    image=Image( format='png',data=png_graph)
    display_png(image)

def showJT ( bn, bgcolor="#6b85d1", fgcolor="#FFFFFF",size="10" ):
    graph = dot.Dot( "jt", graph_type='graph',size=size)
    ie=gum.LazyPropagation(bn)
    jt=ie.junctionTree()
    for cliq in jt.ids():
        graph.add_node(
            dot.Node('"'+str(cliq)+'"',
                       label='"'+",".join([str(i) for i in jt.clique(cliq)])+'"',
                       style="filled",
                       fillcolor="burlywood"
           )
        )
    for edg in jt.edges():
        sep=jt.clique(edg[0]).intersection(jt.clique(edg[1]))
        sepname="{0}+{1}".format(edg[0],edg[1])
        graph.add_node(
            dot.Node(sepname,
                       label='"'+",".join([str(i) for i in sep])+'"',
                       style="filled",
                       shape="box",
                       fillcolor="palegreen",
                       fontsize="8",
                       width="0",
                       height="0"
           )
        )
        graph.add_edge(dot.Edge ( str(edg[0]),sepname))
        graph.add_edge(dot.Edge ( sepname,str(edg[1])))

    # affichage
    png_graph=graph.create_png()
    image=Image( format='png',data=png_graph)
    display_png(image)
   # return image


class inferenceOptimizer:
  def __init__(self,jt,targets={},evs={}):
    self.jt=jt

  def clear(self,targets,evs):
    self.targets=targets
    self.evs=evs
    self.sotargets=set(self.targets.values())
    self.soevs=set(self.evs.values())

    self.roots=set()
    self.dynroots=set()
    self.diffusion=set()
    self.collect=set()

    self.nodeLabels={}
    self.arcLabels={}
    
    self.nbVisitedChild={}
    self.parent={}
    self.flag1={}
    self.flag2={}
    self.m=0

  def showDynamicRoots(self,size="5"):
    g=dot.Dot(graph_type='digraph')
    for i in self.jt.ids():
      if i in self.sotargets and i in self.soevs:
        bgcol="#009999"
        fgcol="#FFFFFF"
      elif i in self.sotargets:
        bgcol="#009900"
        fgcol="#FFFFFF"
      elif i in self.soevs:
        bgcol="#000099"
        fgcol="#FFFFFF"
      else:
        bgcol="#FFFFFF"
        fgcol="#000000"

      width=1
      if i in self.roots:
        pencol="#FF0000"
        width=3
      elif i in self.dynroots:
        pencol="#AAAA00"
        width=2
      else:
        pencol="#000000"

      node=dot.Node(i,style="filled",
                    label="<<B>{0}</B><BR/>{1}>".format(i,str(self.jt.clique(i))),
                    fillcolor=bgcol,
                    fontcolor=fgcol,
                    color=pencol,
                    penwidth=width)
      g.add_node(node)

    for x,y in self.jt.edges():
      lxy=lyx=""
      lxy=self.arcLabels.get((x,y),'')
      lyx=self.arcLabels.get((y,x),'')

      colxy=colyx="#dddddd"
      if (x,y) in self.diffusion:
        colxy="#888844"
      if (y,x) in self.diffusion:
        colyx="#888844"
      if (x,y) in self.collect:
        colxy="#448888"
      if (y,x) in self.collect:
        colyx="#448888"

      g.add_edge(dot.Edge(x,y,label=lxy,color=colxy,fontcolor=colxy))
      g.add_edge(dot.Edge(y,x,label=lyx,color=colyx,fontcolor=colyx))

    showGraph(g,size=size)

  def computeNodeLabel(self,i):
    """
    @return subset of set(["E","T"])
    """
    l=set()
    if i in self.sotargets:
      l.add("T")
    if i in self.soevs:
      l.add("E")
    return l

  def computeArcLabel(self,x,y):
    """
    @return "", "E", "T" ,or "ET"
    """
    l=self.nodeLabels[x].copy()
    for i in self.jt.neighbours(x):
      if i!=y:
        l.add(self.arcLabels[(i,x)])

    if "ET" in l:
      res="ET"
    else:
      res=""
      if "E" in l: res+="E"
      if "T" in l: res+="T"
    return res

  def hasNoETlabel(self,r):
    # assuming that all arcLabels are correct
    for i in self.jt.neighbours(r):
      if self.arcLabels[(i,r)]=='ET':
        return False
    return True


  def distributeFrom(self,r,fro=None):
    if (self.hasNoETlabel(r)):
      return r # root found

    for i in self.jt.neighbours(r):
      if i!=fro:

        if self.arcLabels[(i,r)]=='': # useless to go in branches with no evs/targets
          continue

        self.diffusion.add((r,i))
        self.arcLabels[(r,i)]=self.computeArcLabel(r,i)

        s=(self.arcLabels[(r,i)],self.arcLabels[(i,r)])

        if (s==('ET','ET')): # not useful or (s==('E','T')) or (s==('T','E')):
          return i # root found

        n=self.distributeFrom(i,fro=r)

        if n!=-1: # root found from i
          return n

    return -1 # no root found
  
  def findRoot(self,targets,evs,visible=True):
    self.clear(targets,evs)
    stack=[]
    if len(self.targets)==0 or len(self.evs)==0: return set()
    else:
        for i in self.jt.ids():
          self.nodeLabels[i]=self.computeNodeLabel(i)
          ll=len(self.jt.neighbours(i))
          if ll==0:
            self.dynroots.add(i)
            continue
          if ll==1:
            stack.append((i,self.jt.neighbours(i).pop()))
        while len(stack)>0:
          x,y=stack.pop(0)
    
          if x in self.dynroots: # we do not distribute
            continue
    
          self.arcLabels[(x,y)]=self.computeArcLabel(x,y)
    
          self.collect.add((x,y))
    
          f=[z for z in self.jt.neighbours(y)
             if (y,z) not in self.collect and (z,y) not in self.collect]
          if len(f)==0:
            self.dynroots.add(y)
          elif len(f)==1:
            stack.append((y,f[0]))
        self.markNodes()    
        self.roots=set()
        for r in self.dynroots:
          x=self.distributeFrom(r)
          self.roots.add(x)
          self.flag2[x]=1
    
        if visible:
          self.showDynamicRoots()
        return self.roots

  def adjustCliques(self):
    """
    Assuming that findRoot has been called, adjust cliques for evs and
    targets given the roots
    """
    evs=set(self.evs.keys())
    targets=set(self.targets.keys())
    stack=list(self.roots)
    done=set()

    neoEvs={}
    neoTargets={}
    while len(evs)+len(targets)>0:
      node=stack.pop(0)
      done.add(node)

      ns=self.jt.clique(node)
      for v in ns.intersection(evs):
        neoEvs[v]=node
      for v in ns.intersection(targets):
        neoTargets[v]=node
      evs=evs.difference(ns)
      targets=targets.difference(ns)
      for i in self.jt.neighbours(node):
        if i not in done:
          stack.append(i)

    return neoTargets,neoEvs
  def computeLeaves(self):
      leaves=[]
      for i in self.jt.ids():          
          self.nbVisitedChild[i]=set()
          if i not in self.roots and len(self.jt.neighbours(i)) in [1,0]:
              leaves.append(i)
      return leaves  
  def computeParents(self):
      l=self.computeLeaves()
      while len(l)>0  :
          c=l.pop(0)
          if c not in self.roots:          
              par=list(self.jt.neighbours(c).difference(self.nbVisitedChild[c]))[0]
              self.parent[c]=par  
              self.nbVisitedChild[par].add(c)
              if  len(self.nbVisitedChild[par])==len(self.jt.neighbours(par))-1:
                  l.append(par) 
          
  def markNodes(self):      
      for i in self.jt.ids():
        self.flag2[i]=0
        if 'E' in self.nodeLabels[i]:
            self.flag1[i]=1
        else :
            self.flag1[i]=0      
  def nbNotOptMessages(self):
    return 2*len(self.jt.edges())
  def nbOptMessages(self):
      """
      assuming a call for find roots has been done
      """
      if len(self.targets)==0 or len(self.evs)==0: self.m=0
      else:
          self.computeParents()
          l=self.computeLeaves()
          tCIds=self.targets.values()
          # mCollect
          while len(l)>0:
                c=l.pop(0)  
                if c not in self.roots:
                    par=self.parent[c]
                    if self.flag1[c]==1:
                        self.flag1[par]=1
                        self.m+=1
                    self.nbVisitedChild[par].add(c)
                    if len(self.nbVisitedChild[par])==len(self.jt.neighbours(par))-1:
                        l.append(par) 
           #mDistribute
#Uncomment for integrating more optimization           
          for targ in tCIds:
            t=targ
            child_stack=[]
            while self.flag2[t]!=1:
                child_stack.append(t)
                self.flag2[t]=1
                t=self.parent[t]
                self.m+=1
            if 1 in [self.flag1[i] for i in child_stack+[t]]:
                    for i in child_stack:
                        self.flag1[i]=1   
#            while t!=targ:
#                if sum([self.flag1[p] for p in self.jt.neighbours(t) if p!=child_stack[-1]])!=0 :
#                    self.m+=1
#                    print(t) 
#                t=child_stack.pop()                
      return self.m
########################################################################
def containingCliques(jt,nodeId):
    """
    return a list of clique containing nodeId
    """
    c=[]
    for jtId in jt.ids():
        if nodeId in jt.clique(jtId) :
            c.append(jtId)
    return c
    
def CalculCplx(filename,taux,it2,logname='toDeletet.txt'):
  """
    filename:BN file name
    taux:% mododification list of the BN 
    it2: #iteration for computing measure
    measure: 'mean':  averged gain on it2 iteration
             'std' : standard deviation  on it2 iteration
    logname: to store resultes
  """
  bn=gum.loadBN(filename)
  ie=gum.LazyPropagation(bn)
  jt=ie.junctionTree()
  opt=inferenceOptimizer(jt)
  ids=list(bn.ids())
  bn_size=len(ids)
  nbNotOptMess=opt.nbNotOptMessages()
  means=[]
  stds=[]
  f = open(logname, 'r' )
#  f.write('\n#['+filename +']' + '\t'+str(it2) )
  for tau in taux:
    per=int((tau*bn_size)/100)
#    f.write('\n# ------ percentage '+str(tau)+'\n')
    gain=[]
    for i in range(it2):
        tInCl={} #key are bn nodes, values are cliques in jt
        eInCl={}
        targs=random.sample(ids,per) #
        evs=random.sample(ids,per)       
        for t in targs :
            tInCl[t]=random.choice(containingCliques(jt,t))            
        for e in evs :
            eInCl[e]=random.choice(containingCliques(jt,e))           
        opt.findRoot(targets=tInCl,evs=eInCl,visible=False)
        tt,ee=opt.adjustCliques()
        opt.findRoot(targets=tt,evs=ee,visible=False)
        tt,ee=opt.adjustCliques()
        opt.findRoot(targets=tt,evs=ee,visible=False)  
        nbOptMess=opt.nbOptMessages() 
#        f.write(str(targs)+'\t'+str(evs)+'\t'+str(nbOptMess)+'\t'+str(nbNotOptMess)+'\n')
        gain.append(1-nbOptMess/nbNotOptMess)            
    mm=np.mean(gain,dtype=np.float64)
    ss=np.std(gain,dtype=np.float64)
    means.append(100*mm)
    stds.append(100*ss)
  f.write('x,y,e='+str(taux)+','+str(means)+','+str(stds)+'\n')      
  f.write("plt.errorbar(x, y,e,label=\'{}\'".format(filename[29:-4])+')\n')
#  f.write('x,y='+str(taux)+','+str(means)+'\n')      
#  f.write("plt.plot(x,y,label=\'"+str(bn_size)+"\')\n")
  f.close()
  return means,taux

def gain(filename,pe,it2,logname='toDelete.txt'):
  """
    filename:BN file name
    pe: % mododification of the BN
    it2: #iteration for computing measure mean or std
  """
  bn=gum.loadBN(filename)
  ie=gum.LazyPropagation(bn)
  jt=ie.junctionTree()
  opt=inferenceOptimizer(jt)
  ids=list(bn.ids())
  bn_size=len(ids)
  nbNotOptMess=opt.nbNotOptMessages()
  f = open(logpath+logname+'-{}.csv'.format(it2),'x')
  for p in pe:
      per=int((p*bn_size)/100)
      f.write(str(p)+' ')
      for i in range(it2):
            tInCl={} #key are bn nodes, values are cliques in jt
            eInCl={}
            targs=random.sample(ids,per) #
            evs=random.sample(ids,per)       
            for t in targs :
                tInCl[t]=random.choice(containingCliques(jt,t))            
            for e in evs :
                eInCl[e]=random.choice(containingCliques(jt,e))           
            opt.findRoot(targets=tInCl,evs=eInCl,visible=False)
            tt,ee=opt.adjustCliques()
            opt.findRoot(targets=tt,evs=ee,visible=False)
            tt,ee=opt.adjustCliques()
            opt.findRoot(targets=tt,evs=ee,visible=False)  
            nbOptMess=opt.nbOptMessages() 
            g=100*(1-nbOptMess/nbNotOptMess)
            f.write(str(g)+' ')
      f.write('\n')
  f.close()
  return
  
##########################################################################                 
# TEST 
  ####################################################"
  


#bn=gum.loadBN(resourcespath+'asia.bif')
#ie=gum.LazyPropagation(bn)
#jt=ie.junctionTree()
#opt=inferenceOptimizer(jt)
#
#
#showBN(bn)
#roots=opt.findRoot(targets={2:0,6:4,7:1},evs={4:3,6:4},visible=True)
#t,e=opt.adjustCliques()
#print("targets=",t)
#print("evs=",e)
#roots=opt.findRoot(targets=t,evs=e,visible=True)
#t,e=opt.adjustCliques()
#print("targets=",t)
#print("evs=",e)
#roots=opt.findRoot(targets=t,evs=e,visible=True)
#
# ne pas refaire le test deux fois sinon on va reouvrir les fichier deja existant


#Uncomment for artificial BNs
modif_per=[5,15,30,50,80]

for filename in glob.glob(resourcespath+'/IJTI_gen/veryLarge100-1250/bn201704171935-1050-2*.bif'):
    print(filename[72:-4])
    gain(filename,modif_per,20,filename[72:-4])

#util
#obtain the last line    
#with open('asia-mean-15.txt', 'rb') as fh:
#    for line in fh:
#        pass
#    last=line
## add a line to a first line
#for filename in glob.glob('C:/Dev/aGrUM/urbs/ressources/log_results/realisitc/*.py'):
#    print(filename)
#    with file(filename, 'r') as original: data = original.read()
#    with file(filename, 'w') as modified: modified.write('import matplotlib\nmatplotlib.use("Qt4Agg")\nimport matplotlib.pyplot as plt\nsize2gain={}\nsorted_size2gain=[]\n'+data)