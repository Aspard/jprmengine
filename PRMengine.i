%module(directors="1") PRMengine
%feature("director");

%include "typemaps.i"
%include "std_string.i"
%include "std_vector.i"

%template(DoubleVector) std::vector<double>;

%pragma(java) jniclasscode=%{
  static {
    try {
        System.loadLibrary("PRMengine");
    } catch (UnsatisfiedLinkError e) {
      System.err.println(System.getProperty("java.library.path")+"\n");
      System.err.println("Native code library failed to load. \n" + e);
      System.exit(1);
    }
  }
%}

%{
#include "IncPRMengine.h"
#include "PRMexplorer.h"
%}
%include "src/IncPRMengine.h"
%include "src/PRMexplorer.h"