#ifndef I_PRM_ENGINE_H
#define I_PRM_ENGINE_H

#include <string>

#include <agrum/BN/BayesNet.h>
#include <agrum/BN/inference/lazyPropagation.h>
#include <agrum/PRM/PRMFactory.h>
#include <agrum/PRM/elements/PRMSystem.h>

#include "PRMexplorer.h"

using namespace std;

class IPRMengine {
public:
  virtual void readPRM(string o3prmFileName,string system,string classpath) = 0;
  virtual double getPosterior(string instance, string attribute, int mod) = 0;

  virtual void addInstance(string clazz, string instance) = 0;
  virtual void addInstance(string clazz, string instance,string array) = 0;
  virtual void removeInstance(string instance) = 0;
  
  virtual void addEvidence(string instance, string attribute, int mod) = 0;
  virtual void addEvidence(const string& instance, const string& attribute, const vector<double>& vals) = 0;
  virtual void removeEvidence(string instance, string attribute) = 0;
  virtual void removeAllEvidences() = 0;

  virtual void addTarget(string instance, string attribute) = 0;
  virtual void removeTarget(string instance, string attribute) = 0;

  virtual double testRandomPosterior() = 0;

  virtual void addRelation(string container, string slot,
                   string refered) = 0;
  virtual void changeRelation(string container, string slot,
                      string new_refered) = 0;
  virtual void removeRelation(string container, string slot, string refered)  = 0;
  virtual void instantiateSystem() = 0;

  virtual void _addInstance(string clazz, string instance) = 0;
  virtual void _addInstance(string clazz, string instance,string array) = 0;
  virtual void _removeInstance(string instance) = 0;
  virtual void _addRelation(string container, string slot,
                   string refered) = 0;
  virtual void _changeRelation(string container, string slot,
                      string new_refered) = 0;

  virtual void printNodes() = 0;
  virtual void printAllSystems() = 0;
  virtual void printClasses() = 0;
  virtual void printTypes() = 0;
  virtual void printInterfaces() = 0;
  virtual void printRelations() = 0;
};

#endif // I_PRM_ENGINE_H
