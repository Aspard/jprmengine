#include <iostream>
#include <random>
#include <vector>
#include <tuple>

#include <agrum/BN/BayesNet.h>
#include <agrum/PRM/o3prm/O3prmBNReader.h>
#include <agrum/BN/inference/lazyPropagation.h>
#include <agrum/BN/io/BIF/BIFWriter.h>
#include <agrum/PRM/o3prm/O3prmBNWriter.h>

#include "IncPRMengine.h"
#include "PRMexplorer.h"

using namespace std;

void IncPRMengine::readPRM(string o3prmFileName, string system, string classpath) {
    cout << "Reading " << o3prmFileName << "->" << system << " with classpath <" << classpath << ">"
         << endl;

    try {
        gum::prm::o3prm::O3prmReader<double> reader;
        cout << "Adding classpath : " << classpath << endl;
        reader.addClassPath(classpath);
        cout << "Reading file : " << o3prmFileName << endl;
        reader.readFile(o3prmFileName);
        __prm_factory = new gum::prm::PRMFactory<double>(reader.prm());
        cout << "system: " << system << endl;
        __mainSystem = system;
        __sys = new gum::prm::IncSystem(__prm_factory->prm()->getSystem(__mainSystem));
        __bn = new gum::BayesNet<double>();
        cout << " + creating BayesNetFactory " << endl;
        __bn_factory = new gum::BayesNetFactory<double>(__bn);
        cout << " + generating grounded bn" << endl;
        __sys->groundedBN(*__bn_factory);
        cout << " + launching Lazy Prop" << endl;
        __inf = new gum::LazyPropagation<double>(__bn);
        cout << " + declaring new PRMexplorer" << endl;
        __prm_explorer = new PRMexplorer();
        __prm_explorer->addPRM(__prm_factory, __sys);

        auto nbErr = reader.errors();
        if (nbErr > 0) {
            reader.showErrorCounts();
            reader.showElegantErrorsAndWarnings();
            GUM_ERROR(gum::FatalError, "An error occurred while parsing " << o3prmFileName);
        }
    } catch (gum::Exception &e) {
        GUM_SHOWERROR(e);
        throw (e);
    }
};

void IncPRMengine::addEvidence(string instance, string attribute,
                               int mod) {

    auto attrSafeName = (((__sys->get(instance))).get(attribute)).safeName();
    if (!__evidences.exists(instance + "." + attrSafeName)) {
        __evidences.insert(instance + "." + attrSafeName, mod);
    }
};

void IncPRMengine::addEvidence(const string &instance,
                               const string &attribute,
                               const vector<double> &vals) {
    auto attrSafeName = (((__sys->get(instance))).get(attribute)).safeName();
    //TO_DO
}

void IncPRMengine::removeEvidence(string instance,
                                  string attribute) {

    auto attrSafeName = (((__sys->get(instance))).get(attribute)).safeName();

    if (__evidences.exists(instance + "." + attrSafeName)) {
        __evidences.erase(instance + "." + attrSafeName);
    }
}

void IncPRMengine::removeAllEvidences() {
    __evidences.clear();
}

void IncPRMengine::makeInference() {
    __inf->eraseAllEvidence();
    for (gum::HashTable<string, int>::const_iterator iter = __evidences.cbegin(); iter != __evidences.cend(); ++iter) {
        __inf->addEvidence(iter.key(), iter.val());
    }
    __inf->makeInference();

}

double IncPRMengine::getPosterior(string instance, string attribute,
                                  int mod) {
    // assert mod <= #mod attribute, otherwise it return the first modality
    try {
        auto attrSafeName = (((__sys->get(instance))).get(attribute)).safeName();

        //Avoir une fonction qui fait l'inference Après avoir instancier les choses à instancier SI IL Y A EU DU CHANGEMENT

        makeInference();
        gum::Potential<double> p(__inf->posterior(instance + "." + attrSafeName));

        gum::Instantiation i(p);
        i.setFirst();
        for (int m = 0; m < mod; ++m) {
            i.inc();
        }
        return p[i];
    } catch (gum::Exception &e) {
        GUM_SHOWERROR(e);
        throw;
    }
    return 0;
};

double IncPRMengine::testRandomPosterior() {
    random_device rd;
    mt19937 mt(rd());
    uniform_real_distribution<double> dist(0, 1);
    return dist(mt);
}

void IncPRMengine::addInstance(string clazz, string instance, string array) {
    vector <string> v{clazz, instance, array};
    __toDo.push_back(make_tuple(1, "addInstanceArray", v));
}

void IncPRMengine::addInstance(string clazz, string instance) {
    vector <string> v{clazz, instance};
    __toDo.push_back(make_tuple(1, "addInstance", v));
}

void IncPRMengine::addInstanceOnce(string clazz, string instance, string array) {
    vector <string> v{clazz, instance, array};
    __toDo.push_back(make_tuple(0, "addInstanceArray", v));
}

void IncPRMengine::addInstanceOnce(string clazz, string instance) {
    vector <string> v{clazz, instance};
    __toDo.push_back(make_tuple(0, "addInstance", v));
}

void IncPRMengine::_addInstance(string clazz, string instance, string array) {
    //cout << " + adding instance " << instance << " of class " << clazz +" to the array "+array<<endl;
    __sys->addObject((__prm_factory->prm())->getClass(clazz), instance, array);
    //cout << " - done " << endl;
};

void IncPRMengine::_addInstance(string clazz, string instance) {
    //cout << " + adding instance " << instance << " of class " << clazz << endl;
    __sys->addObject((__prm_factory->prm())->getClass(clazz), instance);
    //cout << " - done " << endl;
};

void IncPRMengine::removeInstance(string instance) {
    vector <string> v{instance};
    __toDo.push_back(make_tuple(1, "removeInstance", v));
};

void IncPRMengine::removeInstanceOnce(string instance) {
    vector <string> v{instance};
    __toDo.push_back(make_tuple(0, "removeInstance", v));
};

void IncPRMengine::_removeInstance(string instance) {
    __sys->removeObject(instance);
    __sys->adjust();
};

void IncPRMengine::addTarget(string instance, string attribute) {
};

void IncPRMengine::removeTarget(string instance, string attribute) {
};

void IncPRMengine::addRelation(string container, string slot, string referred) {
    vector <string> v{container, slot, referred};
    __toDo.push_back(make_tuple(1, "addRelation", v));
}

void IncPRMengine::addRelationOnce(string container, string slot, string referred) {
    vector <string> v{container, slot, referred};
    __toDo.push_back(make_tuple(0, "addRelation", v));
}


void IncPRMengine::_addRelation(string container, string slot,
                                string referred) {
    try {
        __sys->setReferenceSlot(container, slot, referred);
    } catch (gum::Exception &e) {
        GUM_SHOWERROR(e);
        throw (e);
    }
};

void IncPRMengine::changeRelation(string container, string slot,
                                  string referred) {
};

void IncPRMengine::_changeRelation(string container, string slot,
                                   string referred) {
};

void IncPRMengine::removeRelation(string container, string slot, string referred) {
    list < tuple < int, string, vector < string >> > __toDoNew;

    bool isRemoved = false;

    for (auto toDo : __toDo) {
        int once = get<0>(toDo);
        string type = get<1>(toDo);
        vector <string> params = get<2>(toDo);

        if (type == "addRelation") {
            if (params.at(0) == container && params.at(1) == slot && params.at(2) == referred) {
                //removing the relation by not copying it
                isRemoved = true;
            } else {
                __toDoNew.push_back(toDo);
            }
        } else {
            __toDoNew.push_back(toDo);
        }
    }

    if (!isRemoved) {
        //throw invalid_argument("Error when removing "+referred+" from "+container+"."+slot);
        cout << "No relation between " + referred + " and " + container + "." + slot << endl;
    }
    __toDo = __toDoNew;
};

void IncPRMengine::instantiateSystem() {
    //A CHANGER ?

    __sys = new gum::prm::IncSystem(__prm_factory->prm()->getSystem(__mainSystem));
    //__sys = __sys2;

    list < tuple < int, string, vector < string >> > __toDoNew;

    for (auto toDo : __toDo) {
        int once = get<0>(toDo);
        string type = get<1>(toDo);
        vector <string> params = get<2>(toDo);

        if (type == "addInstance") {
            _addInstance(params.at(0), params.at(1));
        }

        if (type == "addInstanceArray") {
            _addInstance(params.at(0), params.at(1), params.at(2));
        }

        if (type == "addRelation") {
            _addRelation(params.at(0), params.at(1), params.at(2));
        }

        if (type == "removeInstance") {
            _removeInstance(params.at(0));
        }

        if (once == 1) {
            __toDoNew.push_back(toDo);
        }
    }

    __toDo = __toDoNew;

    //__toDoOnce.clear();

    __sys->adjust();
    __bn = new gum::BayesNet<double>();
    __bn_factory = new gum::BayesNetFactory<double>(__bn);
    __sys->groundedBN(*__bn_factory);
    delete __inf;
    __inf = new gum::LazyPropagation<double>(__bn);

    __prm_explorer = new PRMexplorer();
    __prm_explorer->addPRM(__prm_factory, __sys);
};

void IncPRMengine::printClasses() {
    for (string cl : __prm_explorer->classes()) {
        cout << "Class : " << cl << endl;

        if (__prm_explorer->getSuperClass(cl) == "") {
            cout << "  - Super class : None" << endl;
        } else {
            cout << "  - Super class : " << __prm_explorer->getSuperClass(cl) << endl;
        }

        cout << "  - Implemented interface : " << endl;

        for (auto inter : __prm_explorer->classImplements(cl)) {
            cout << "\t" << inter << endl;
        }

        cout << "  - Direct sub-types : " << endl;

        for (auto ext : __prm_explorer->getDirectSubClass(cl)) {
            cout << "\t" << ext << endl;
        }

        cout << "  - Attributes : " << endl;

        for (auto v : __prm_explorer->classAttributes(cl)) {
            cout << "\t" << get<0>(v) << " " << get<1>(v) << " (" << get<2>(v) << ")" << endl;
        }

        cout << "  - References : " << endl;

        for (auto v : __prm_explorer->classReferences(cl)) {
            if (get<2>(v)) {
                cout << "\t" << get<0>(v) << ("[]") << " " << get<1>(v) << endl;
            } else {
                cout << "\t" << get<0>(v) << " " << get<1>(v) << endl;
            }
        }

        cout << "  - Aggragates : " << endl;

        for (auto v : __prm_explorer->classAggregates(cl)) {
            if (get<3>(v) == "") {
                cout << "\t" << get<0>(v) << " " + get<1>(v) << " " << get<2>(v) << " NoLabel (" << get<4>(v) << ")"
                     << endl;
            } else {
                cout << "\t" << get<0>(v) << " " + get<1>(v) << " " << get<2>(v) << " " << get<3>(v) << " ("
                     << get<4>(v) << ")"
                     << endl;
            }
        }

        cout << "  - SlotChains : " << endl;

        for (auto v : __prm_explorer->classSlotChains(cl)) {
            if (get<2>(v)) {
                cout << "\t" << get<0>(v) << " " << get<1>(v) << " []" << endl;
            } else {
                cout << "\t" << get<0>(v) << " " << get<1>(v) << endl;
            }
        }

        cout << "  - Parameters : " << endl;

        for (string param : __prm_explorer->classParameters(cl)) {
            cout << "\t" + param << endl;
        }
        cout << "" << endl;
    }

    cout << "" << endl;
}

void IncPRMengine::printAllSystems() {
    cout << "The following lists the systems of the prm:\n" << endl;

    vector < tuple < string, map < unsigned long, vector < string >>, vector < tuple < unsigned long,
            unsigned long >> >> systems = __prm_explorer->getalltheSystems();

    for (auto system : systems) {

        cout << "Name of the system: " + get<0>(system) + "\n" << endl;
        cout << "Nodes : id: [name,type])" << endl;

        map<unsigned long, std::vector<string>> dict = get<1>(system);
        map < unsigned long, std::vector < string >> ::iterator
        it = dict.begin();

        while (it != dict.end()) {
            cout << "\t" << it->first << ": " << it->second << endl;
            it++;
        }

        cout << "" << endl;

        vector <tuple<unsigned long, unsigned long>> arcs = get<2>(system);
        cout << "Arcs : List[(tail, head),(tail, head)...]" << endl;

        for (auto &arc : arcs) {
            cout << "(" << get<0>(arc) << "," << get<1>(arc) << ")" << endl;
        }
    }

    cout << "" << endl;
}

void IncPRMengine::printCurrentSystem() {
    tuple < string, map < unsigned long, vector < string >>, vector < tuple < unsigned long,
            unsigned long >> > system = __prm_explorer->getTheMainSystem();

    cout << "The following lists the systems of the prm:\n" << endl;
    cout << "Name of the system: " + get<0>(system) + "\n" << endl;
    cout << "Nodes : id: [name,type])" << endl;

    map<unsigned long, std::vector<string>> dict = get<1>(system);
    map < unsigned long, std::vector < string >> ::iterator
    it = dict.begin();

    while (it != dict.end()) {
        cout << "\t" << it->first << ": " << it->second << endl;
        it++;
    }

    cout << "" << endl;

    vector <tuple<unsigned long, unsigned long>> arcs = get<2>(system);
    cout << "Arcs : List[(tail, head),(tail, head)...]" << endl;

    for (auto &arc : arcs) {
        cout << "(" << get<0>(arc) << "," << get<1>(arc) << ")" << endl;
    }

    cout << "" << endl;
}

void IncPRMengine::printNodes() {
    cout << "The following lists the nodes of the prm:\n" << endl;

    //tuple<string,map<unsigned long,vector<string>>,vector<tuple<unsigned long,unsigned long>>> system = __prm_explorer->getTheMainSystem();
    tuple < string, map < unsigned long, vector < string >>, vector < tuple < unsigned long,
            unsigned long >> > system = __prm_explorer->getTheMainSystem();

    cout << "Name of the system: " + get<0>(system) + "\n" << endl;
    cout << "Nodes : id: [name,type])" << endl;

    map<unsigned long, std::vector<string>> dict = get<1>(system);
    map < unsigned long, std::vector < string >> ::iterator
    it = dict.begin();

    while (it != dict.end()) {
        cout << "\t" << it->first << ": " << it->second << endl;
        it++;

    }

    cout << "" << endl;
}

void IncPRMengine::printTypes() {
    for (auto cl : __prm_explorer->types()) {
        cout << "Type : " << cl << endl;
        if (__prm_explorer->getSuperType(cl) == "") {
            cout << "  - Super type : None" << endl;
        } else {
            cout << "  - Super Type : " << __prm_explorer->getSuperType(cl) << endl;
        }

        cout << "  - Direct sub-types : " << endl;
        for (auto name : __prm_explorer->getDirectSubTypes(cl)) {
            cout << "\t" << name << endl;
        }

        cout << "  - Labels : " << endl;

        for (auto t : __prm_explorer->getLabels(cl)) {
            cout << "\t" << t << endl;
        }

        cout << "  - Labels mapping : " << endl;

        pair<map<string, string>, bool> pairLabelMap = __prm_explorer->getLabelMap(cl);

        bool hasLabelMap = get<1>(pairLabelMap);

        if (hasLabelMap) {
            map <string, string> labelMap = get<0>(pairLabelMap);
            map<string, string>::iterator it = labelMap.begin();

            while (it != labelMap.end()) {
                cout << "\t" << it->first << " -> " << it->second << endl;

                it++;
            }
        }
    }

    cout << "" << endl;
}

void IncPRMengine::printInterfaces() {
    for (auto cl : __prm_explorer->interfaces()) {

        cout << "Interface : " + cl << endl;

        if (__prm_explorer->getSuperInterface(cl) == "") {
            cout << "  - Super interface : None" << endl;
        } else {
            cout << "  - Super interface : " << __prm_explorer->getSuperInterface(cl) << endl;
        }

        cout << "  - Direct sub-interfaces : " << endl;

        for (auto name : __prm_explorer->getDirectSubInterfaces(cl)) {
            cout << "\t" + name << endl;
        }

        cout << "  - Implementations : " << endl;

        for (auto impl : __prm_explorer->getImplementations(cl)) {
            cout << "\t" + impl << endl;
        }

        cout << "  - Attributes : " << endl;

        for (auto t : __prm_explorer->interAttributes(cl, true)) {
            cout << "\t" + get<0>(t) + " " + get<1>(t) << endl;
        }

        cout << "  - References : " << endl;

        for (auto t : __prm_explorer->interReferences(cl)) {
            if (get<2>(t)) {
                cout << "\t" << get<0>(t) << "[] " << get<1>(t) << endl;
            } else {
                cout << "\t" << get<0>(t) << " " << get<1>(t) << endl;
            }
        }

        cout << "" << endl;
    }
}

void IncPRMengine::printRelations() {
    try {
        cout << "Relations : " << endl;

        for (gum::Arc arc : __prm_explorer->getMainSystemArcs()) {
            cout << "head : " << arc.head() << ", tail : " << arc.tail() << endl;
        }

        cout << "" << endl;

    } catch (gum::Exception &e) {
        GUM_SHOWERROR(e);
        throw (e);
    }
}

void IncPRMengine::saveO3PRM(string filename) {

    auto writer = gum::O3prmBNWriter<double>();

    try {
        // This will print the asia BayesNet on the standard output stream
        writer.write(cout, *__bn);
        // This will write the asia BayesNet in the given file
        writer.write(filename, *__bn);
    } catch (gum::IOError &e) {
        //
    }
}

void IncPRMengine::saveBN(string filename) {

    auto writer = gum::BIFWriter<double>();

    try {
        // This will print the asia BayesNet on the standard output stream
        writer.write(cout, *__bn);
        // This will write the asia BayesNet in the given file
        writer.write(filename, *__bn);
    } catch (gum::IOError &e) {
        //
    }
}

void IncPRMengine::printBN() {

    instantiateSystem();

    auto writer = gum::BIFWriter<double>();

    try {
        // This will print the asia BayesNet on the standard output stream
        writer.write(cout, *__bn);
    } catch (gum::IOError &e) {
        //
    }
}