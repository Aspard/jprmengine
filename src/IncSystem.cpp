#include "IncSystem.h"

#include <agrum/PRM/elements/PRMInstance.h>

#include <agrum/multidim/aggregators/min.h>
#include <agrum/multidim/aggregators/max.h>
#include <agrum/multidim/aggregators/exists.h>
#include <agrum/multidim/aggregators/forall.h>


using namespace std;

namespace gum{
  namespace prm{

  	/*

	Changer les fonctions pour ne plus stocker que des listes
	1) les éléments à instancier
	2) ceux à supprimer
	3) les relations à ajouter
	4) les relations à enlever

	On ne fait plus d'adjust, on refait un modèle from scratch à chaque fois
	-> on perd sûrement en efficacité mais c'est sûrement marginal
  	*/

    IncSystem::IncSystem(const PRMSystem<double>& syst ): PRMSystem<double>::PRMSystem(syst) {
        GUM_CONSTRUCTOR( IncSystem );
    }

    IncSystem::~IncSystem() {
        GUM_DESTRUCTOR( IncSystem );
        for ( const auto& elt : *this )
            delete elt.second;

        for ( const auto& elt : _instanceMap )
            delete elt.second;

        for ( const auto& elt : _arrayMap )
            delete elt.second.second;
    }


    void IncSystem::__removeNotReferredObj( const gum::prm::PRMInstance<double>& i) {
        NodeId id = _nodeIdMap.keyByVal( const_cast<gum::prm::PRMInstance<double>*>( &i ) );
        (_instanceMap[const_cast<gum::prm::PRMClass<double>*>(&(i.type()))])->erase(const_cast<gum::prm::PRMInstance<double>*>(&i));
        _nameMap.erase(i.name());
        _nodeIdMap.erase(id);
        _skeleton.eraseNode(const_cast<NodeId&>(id));
        delete ( const_cast<gum::prm::PRMInstance<double>*>( &i ));
        ///TODO  if (isArray(toDelete_name)){delete __arrayMap[toDelete_name].second.second;}
    }

    bool IncSystem::__isReferred(const gum::prm::PRMInstance<double>& instance) {
        for( auto id:instance.type().dag().nodes()) {
            if (instance.hasRefAttr(id)) {
                return true;
            }
        }
        return false;
    }

    /**
     use this if toDelete_name is referenced, then give a valid
     reference to replace it if not an agg or when it's the only
     item in the agr,
     TODO: once can optimize with one method having optional param
    */

    void IncSystem::removeObject( const string& toDelete_name, const string& newObj_name) {

        try {
            const gum::prm::PRMInstance<double>& toDelete_instance=get(toDelete_name);
            if(not __isReferred(toDelete_instance)) {
                __removeNotReferredObj(toDelete_instance);
            } else {
                try {
                   // gum::prm::Instance<double>& j=this->get(newObj_name);

                    // __referingAttr.begin()
                    // get pr's reference slots
                    // change in pr.swap(i,j): browse __referenceMap, __referringAttr  and change i with j
                    for (const auto &inst:*this ) {
                        GUM_TRACE_VAR(inst.second->name())
                        for(const auto& n:inst.second->type().dag().nodes()) {
                            inst.second->getInstances(n);
                            if (inst.second->type().get(const_cast<NodeId&> (n)).elt_type()!= gum::prm::PRMClassElement<double>::prm_slotchain
                                    and 
                                    inst.second->getInstances(n).exists(const_cast<gum::prm::PRMInstance<double>*>(&toDelete_instance))) {
                              //do something ?
                            }
                        }

                    }

                } catch(NotFound& e) {
                    cerr<<"please provide a valid substitute instance name: "<< newObj_name<<" was not found !" <<endl;
                    throw;
                }


            }
            if(toInstantiate.exists(const_cast<gum::prm::PRMInstance<double>*>(&toDelete_instance))) {
                toInstantiate.eraseByVal(( const_cast<gum::prm::PRMInstance<double>*>(&toDelete_instance)));
            }
        } catch(Exception& e) {
            cerr<<"Instance to delete not found: "<< toDelete_name<<endl;
            throw;
        }
    }

    void IncSystem::addObject(const gum::prm::PRMClass< double > & type,
                              const   string& object_name ) {

        //cout << " Adding Object for "  << object_name << endl;

      gum::prm::PRMInstance<double>* obj = new gum::prm::PRMInstance<double>( object_name,const_cast<gum::prm::PRMClass<double>&>(type));

      try {

        add( obj );

        toInstantiate.insert(obj);
      } catch ( OperationNotAllowed & e ) {
        if ( obj ) {
      	    delete obj;
        }
    	  throw;
      }
    }

    void IncSystem::addObject(const  gum::prm::PRMClass< double > & type,
                      			 	const string& object_name,
               								const string& array){

    	//cout << " Adding Object for "  << object_name << " in "
    	//        << array << endl;

    	gum::prm::PRMInstance<double>* obj = new gum::prm::PRMInstance<double>( object_name,const_cast<gum::prm::PRMClass<double>&>(type));
    	try {
    		try {
    			add(array,obj );
    		} catch ( gum::Exception& e ) { GUM_SHOWERROR(e); throw e; }
    		toInstantiate.insert(obj);

    	} catch ( OperationNotAllowed & e ) {
    		if ( obj ) {
    			delete obj;
    		}
    			throw;
    		}
    	
    }


  ///TODO: must delete previous reference if not agg (by using size of map) and ensure
  ///every instance is instantiated in the system
  /// manage error if not all ref are instantiated !!
  /// pre-condition: already instantiated references !
    void IncSystem::setReferenceSlot(
        const string& object_name,
        const string& refField_name,
        const string& ref_name) {

        vector<gum::prm::PRMInstance<double>*> lefts;
        vector<gum::prm::PRMInstance<double>*> rights;
        //cout << " Setting Reference Slot for "  << object_name << ", "
        //    << refField_name << ", " << ref_name << endl;
        //cout << endl;

        if ( isInstance( object_name ) ) {
            lefts.push_back( &( get( object_name ) ) );
        } else if ( isArray( object_name ) ) {
            for ( const auto& elt : getArray( object_name ) )
                lefts.push_back( elt );
        } else {
            GUM_ERROR( NotFound,
                       "left value does not name an instance or an array" );
        }

        if ( isInstance( ref_name ) ) {
            rights.push_back( &( get( ref_name ) ) );
        } else if ( isArray( ref_name ) ) {
            for ( const auto& elt : getArray( ref_name ) )
                rights.push_back( elt );
        } else {
            GUM_ERROR( NotFound,
                       "right value does not name an instance or an array" );
        }


        for ( const auto l : lefts ) {
          for ( const auto r : rights ) {
            if ( ( *l ).type().get( refField_name ).elt_type() ==
                    gum::prm::PRMClassElement<double>::prm_refslot ) {

              (*l).add((*l).type().get(refField_name).id(), *r);
            } else {
              GUM_ERROR( NotFound, "unfound reference slot" );
            }
          }
        }  
    }

    /**
    @require all references are declared and put in toInstantiate !
    @require all reference are set in the instance to instantiate !
    /// TODO review this method in order for it to set all needed reference and instantiate then !
    */
    void IncSystem::adjust() {
      for (auto instance:toInstantiate) {
          (*instance).instantiate();
      }

      toInstantiate.clear();	
    }
  }
}
