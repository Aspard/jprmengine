
#include <iostream>
#include <random>

#include <agrum/BN/BayesNet.h>
#include <agrum/PRM/o3prm/O3prmBNReader.h>
#include <agrum/BN/inference/lazyPropagation.h>

#include "BasicPRMengine.h"

void BasicPRMengine::readPRM(std::string o3prmFileName, std::string system,  std::string classpath) {
  std::cout << "Reading " << o3prmFileName << "->"<<system<< " with classpath <" << classpath <<">"
            << std::endl;

  try {
    gum::prm::o3prm::O3prmReader<double> reader;
    reader.addClassPath(classpath);
    reader.readFile( o3prmFileName) ;
    //__prm_factory= new gum::prm::IncPRMFactory(reader.prm());
    __prm_factory= new gum::prm::PRMFactory<double>(reader.prm());
    std::cout << "system: " << system << std::endl;
    __sys = new gum::prm::IncSystem(__prm_factory->prm()->getSystem(system));
    //__sys = new gum::prm::PRMSystem<double>(__prm_factory->prm()->getSystem(system));
    __bn_factory= new gum::BayesNetFactory<double>(new gum::BayesNet<double>());
    __sys->groundedBN( *__bn_factory );
    __inf= new gum::LazyPropagation<double>(__bn_factory->bayesNet());
    auto nbErr=reader.errors();
    if (nbErr>0) {
        reader.showErrorCounts();
        reader.showElegantErrorsAndWarnings();
        GUM_ERROR(gum::FatalError,"An error occurred while parsing "<<o3prmFileName);
    }
  } catch (gum::Exception& e) {
    GUM_SHOWERROR(e); 
	throw(e);
  }
};

/*
void BasicPRMengine::readPRM(std::string o3prmFileName, std::string system, std::string classpath) {
  //std::cout << "Reading " << o3prmFileName << "->"<<system<< " with classpath <" << classpath <<">"
  //          << std::endl;


  try {
      gum::O3prmBNReader<double> reader(&__bn,o3prmFileName,system,classpath);

      auto nbErr=reader.proceed();
      if (nbErr>0) {
          reader.showErrorCounts();
          reader.showElegantErrorsAndWarnings();
          GUM_ERROR(gum::FatalError,"An error occured while parsing "<<o3prmFileName);
      }
  } catch (gum::IOError& e) {
    throw(e);
  }

  //std::cout<<__bn<<std::endl;
  __inf=new gum::LazyPropagation<double>(&__bn);
}*/

void BasicPRMengine::addEvidence(std::string instance, std::string attribute,
                                 int mod, bool verbose = false) {

	auto attrSafeName= (((__sys->get(instance))).get(attribute)).safeName();
	if(verbose) {
      std::cout << "Posterior before evidence :"
                << __inf->posterior((__inf->BN()).idFromName(instance + "." + attrSafeName)) << std::endl;
    }
    __inf->addEvidence((__inf->BN()).idFromName(instance+"."+attrSafeName),mod);
    __inf->makeInference();
    if(verbose) {
      std::cout << " + evidence added on " << (instance+"."+attribute) << "[" << mod << "]" << std::endl;
      std::cout << "Posterior after evidence :" << __inf->posterior((__inf->BN()).idFromName(instance+"."+attrSafeName)) << std::endl;
      std::cout << std::endl;
    }
	
	/*gum::Potential<double> ev;
	gum::List<gum::Potential<double> const*> evidences;
    ev << (__inf->bn()).variableFromName(attrSafeName );
	ev.fillWith( {0.0, 1.0} );	  
	evidences.insert( &ev );
    __inf->insertEvidence( evidences );
   __inf->makeInference();*/
	
	//

};

void BasicPRMengine::addEvidence(const std::string& instance,
                                 const std::string& attribute,
                                 const std::vector<double>& vals,
                                 bool verbose = false){

    if(verbose) {
      std::cout << " + adding evidence on " << instance << "." << attribute << "["
                << mod << "]" << std::endl;
    }

    auto attrSafeName= (((__sys->get(instance))).get(attribute)).safeName();
    __inf->addEvidence((__inf->BN()).idFromName(instance+"."+attrSafeName),vals);
    __inf->makeInference();
}

void BasicPRMengine::removeEvidence(std::string instance,
                                    std::string attribute,
                                    bool verbose = false) {
									
	auto attrSafeName= (((__sys->get(instance))).get(attribute)).safeName();
	__inf->eraseEvidence((__inf->BN()).idFromName(instance+"."+attrSafeName));
	__inf->makeInference();

	if(verbose){
      std::cout << " +  evidence removed from " << instance << "." << attribute<< std::endl;
	}
    //std::cout << __inf->posterior((__inf->BN()).idFromName(instance+"."+attrSafeName)) << std::endl;

}
void BasicPRMengine::removeAllEvidences(bool verbose = false) {
	__inf->eraseAllEvidence();
	__inf->makeInference();
	if(verbose){
	  std::cout << " + all evidences removed " << std:endl;
	}
}

double BasicPRMengine::getPosterior(std::string instance,
                                    std::string attribute,
                                    int mod,
                                    bool verbose = false) {
    // assert mod <= #mod attribute, otherwise it return the first modality
	auto attrSafeName= (((__sys->get(instance))).get(attribute)).safeName();
    __inf->makeInference();
    gum::Potential<double> p(__inf->posterior((__inf->BN()).idFromName(instance+"."+attrSafeName)));
    gum::Instantiation i( p );
    i.setFirst();
    for (int m=0;m<mod;++m){
    	i.inc();
    }

    if(verbose){
      std::cout << "Posterior for " << instance  << "." << attribute << "==" << mod << " is " << p[i] << "." << std::endl;
    }

    return p[i];
};

double BasicPRMengine::testRandomPosterior() {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(0, 1);
    return dist(mt);
}

void BasicPRMengine::addInstance(std::string clazz, std::string instance,std::string array){
	
	__sys->addObject((__prm_factory->prm())->getClass(clazz),instance,array);
	//std::cout << " + adding instance " << instance << " of class " << clazz +" to the array "+array<<std::endl;
};

void BasicPRMengine::addInstance(std::string clazz, std::string instance) {
    __sys->addObject( (__prm_factory->prm())->getClass(clazz), instance);
	//std::cout << " + adding instance " << instance << " of class " << clazz << std::endl;
};

void BasicPRMengine::instantiateSystem(bool verbose = false){
    
	__sys->adjust();
	__bn_factory= new gum::BayesNetFactory<double>( new gum::BayesNet<double>());
    __sys->groundedBN( *__bn_factory );
	delete __inf;
    __inf= new gum::LazyPropagation<double>(__bn_factory->bayesNet());
    //__inf= new gum::LazyPropagation<double>(*(__bn_factory->bayesNet());
    if(verbose) {
      std::cout << "system instantiated"<< std::endl; //+factory.bayesNet()->toDot()
    }
};

void BasicPRMengine::removeInstance(std::string instance,
                                    bool verbose = false) {
    if(verbose) {
      std::cout << " + removing instance " << instance << std::endl;
    }
    __sys->removeObject(instance);
    __sys->adjust();

    if(verbose) {
      std::cout << "system after removing\n " + factory.bayesNet()->toDot() << std::endl;
    }
};

void BasicPRMengine::addTarget(std::string instance, std::string attribute,
                               bool verbose = false) {
    //std::cout << " + adding target on " << instance << "." << attribute
    //          << std::endl;
};

void BasicPRMengine::removeTarget(std::string instance, std::string attribute,
                                  bool verbose = false) {
    //std::cout << " + removing target on " << instance << "." << attribute
    //          << std::endl;
};

void BasicPRMengine::addRelation(std::string container, std::string slot,
                                 std::string referred,
                                 bool verbose = false) {
  	// assume the relation 	exists not before !
    if(verbose) {
      std::cout << " + adding relation : " << container << "." << slot
                << "+=" << referred << std::endl;
    }
	__sys->setReferenceSlot(container,slot,referred);
    if(verbose) {
	  std::cout << "Relation added" << std::endl;
    }
};

void BasicPRMengine::changeRelation(std::string container, std::string slot,
                                    std::string referred,
                                    bool verbose = false) {
    //std::cout << " + changing relation : " << container << "." << slot
    //          << "+=" << referred << std::endl;
};

void BasicPRMengine::removeRelation(std::string container, std::string slot,
                                    bool verbose = false) {
    //std::cout << "+ removing relation : " << container << "." << slot
    //          << std::endl;
};
