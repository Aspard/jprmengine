#ifndef INC_SYSTEM_H
#define INC_SYSTEM_H

#include<agrum/PRM/elements/PRMSystem.h>

namespace gum{
  namespace prm{

    class IncSystem : public PRMSystem<double> {
          /// list of instances impacted by incremental changes and need to be instantiate to make the system consistent
      public:
          List<PRMInstance<double>*> toInstantiate;
          IncSystem( const PRMSystem<double>& syst);
          virtual ~IncSystem();
          //IncSystem& operator= (const PRMSystem<double>& s );
          ///TODO create

          ///&(const_cast<PRM&>(skelelton))
          /// use this method if toDelete_name is not referenced
          /// you may update inverse slot chain here

          List<  PRMInstance<double>*> getImpactedInsances() {
              return toInstantiate;
          }

          /// Incremental operations
          /**
          when you add an object, think to set its reference
          next
          */
          void addObject(const PRMClass<double>& type,
                         const std::string& object_name);

          void addObject(const PRMClass<double>& type,
                         const std::string& object_name,
                         const std::string& array);

          /**
           use this if toDelete_name is referenced, then give a valid
           reference to replace it if not an agg or when it's the only
           item in the agr,
           TODO: once can optimize with one method having optional param
          */

          void removeObject(const std::string& toDelete_name,
                            const std::string& newObj_name="");


          void setReferenceSlot(
              const std::string& object_name,
              const std::string& refField_name ,
              const std::string& ref_name );

          void removeReference(
              const std::string& object_name,
              const std::string& refField_name,
              const std::string& ref_name );

          void getReference(
              const std::string& object_name,
              const std::string& refField_name ,
              const std::string& ref_name );

          /**
          call this method before inference to make sure that current system is consistent after changes
          */
          void adjust();

      private:
          void __removeNotReferredObj(  const  PRMInstance<double>& i );
          /// true if instance is referred
          bool __isReferred(const  PRMInstance<double>& instance);

          void __toRemove();
          void __toAdd();

    };
  }
} /* namespace gum */
#endif  // INC_SYSTEM
