#ifndef EVIDENCE_H
#define EVIDENCE_H

#include <string>

class Evidence {
	std::string attrSafeName;
	std::vector<double>& vals;

public:
	std::vector<double> getEvidence();
	std::string getAttributeSafeName();
	void setValue(std::vector<double>& vals);
	void setName(std::string attrSafeName);

};

#endif 