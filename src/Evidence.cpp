#include "Evidence.h"

Evidence::Evidence(std::string name, std::vector<double> values){
	attrSafeName = name;
	vals = values;
}

std::vector<double> Evidence::getEvidence();
std::string Evidence::getAttributeSafeName();
void Evidence::setValue(std::vector<double>& vals);
void Evidence::setName(std::string attrSafeName);