#ifndef PRM_EXPLORER_H
#define PRM_EXPLORER_H

#include <string>
#include <vector>
#include <map>

#include <agrum/PRM/PRM.h>
#include <agrum/PRM/elements/PRMAggregate.h>
#include <agrum/PRM/elements/PRMClassElement.h>
#include <agrum/PRM/o3prm/O3prmReader.h>

#include "IncSystem.h"

template < typename GUM_SCALAR >
class classElement;

template < typename GUM_SCALAR >
class Aggregate;

using namespace std;

class PRMexplorer {
  public:
    void load(string filename, string classpath = "", bool verbose = false);
    void addPRM(gum::prm::PRMFactory<double>* prmFactory, gum::prm::IncSystem* mainSys);
    bool isType(string name);
    bool isClass(string name);
    bool isInterface(string name);
    gum::ArcSet getMainSystemArcs();

    /************CLASS**************/

    vector<string> classes();
    vector<tuple<string,string,vector<string>>> classAttributes(string class_name);
    bool isAttribute(string class_name, string att_name);
    vector<tuple<string,string,bool>> classReferences(string class_name);
    vector<string> classParameters(string class_name);
    vector<string> classImplements(string class_name);
    vector<tuple<string,string,string,string,vector<string>>> classAggregates(string class_name);
    vector<tuple<string,string,bool>> classSlotChains(string class_name);
    tuple<map<unsigned long,string>,string> classDag(string class_name);
    vector<tuple<string,map<unsigned long,vector<string>>,vector<tuple<unsigned long,unsigned long>>>> getalltheSystems();
    tuple<string,map<unsigned long,vector<string>>,vector<tuple<unsigned long,unsigned long>>> getTheMainSystem();
    string getSuperClass(string class_name);
    vector<string> getDirectSubClass(string class_name);
    const gum::Potential< double >& cpf(string class_name,string attribute);

    /************TYPE**************/

    vector<string> types();
    string getSuperType(string type_name);
    vector<string> getDirectSubTypes(string type_name);
    vector<string> getLabels(string type_name);
    pair<map<string,string>,bool> getLabelMap(string type_name);

    /************INTERFACE**************/

    vector<string> interfaces();
    vector<tuple<string,string>> interAttributes(string interface_name,bool allAttributes = false);
    vector<tuple<string,string,bool>> interReferences(string interface_name);
    string getSuperInterface(string interface_name);
    vector<string> getDirectSubInterfaces(string interface_name);
    vector<string> getImplementations(string interface_name);

  private:
    gum::prm::PRM< double >* __prm;
    gum::prm::o3prm::O3prmReader< double > __reader;
    gum::prm::IncSystem* __main_system;
};

#endif // PRM_EXPLORER_H