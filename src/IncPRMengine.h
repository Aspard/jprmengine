#ifndef INC_PRM_ENGINE_H
#define INC_PRM_ENGINE_H

#include <string>
#include <vector>
#include <list>

#include <agrum/core/hashTable.h>
#include <agrum/BN/BayesNet.h>
#include <agrum/BN/inference/lazyPropagation.h>
#include <agrum/PRM/PRMFactory.h>
#include <agrum/PRM/elements/PRMSystem.h>

#include "IPRMengine.h"
#include "IncSystem.h"
#include "PRMexplorer.h"
//#include "Evidence.h"

using namespace std;

class IncPRMengine : public IPRMengine {
public:
  virtual void readPRM(string o3prmFileName,string system,string classpath);
  virtual double getPosterior(string instance, string attribute, int mod);

  virtual void addInstance(string clazz, string instance);
  virtual void addInstance(string clazz, string instance,string array);
  virtual void addInstanceOnce(std::string clazz, std::string instance);
  virtual void addInstanceOnce(std::string clazz, std::string instance, std::string array);

  virtual void _addInstance(string clazz, string instance);
  virtual void _addInstance(string clazz, string instance,string array);

  virtual void removeInstanceOnce(std::string clazz);
  virtual void removeInstance(string instance);
  virtual void _removeInstance(string instance);
  
  virtual void addEvidence(string instance, string attribute, int mod);
  virtual void addEvidence(const string& instance, const string& attribute, const vector<double>& vals);
  
  virtual void removeEvidence(string instance, string attribute);
  virtual void removeAllEvidences();

  virtual void makeInference();

  virtual void addTarget(string instance, string attribute);
  virtual void removeTarget(string instance, string attribute);

  virtual double testRandomPosterior();

  virtual void addRelationOnce(string container, string slot, string referred);
  virtual void addRelation(string container, string slot,
                   string referred);
  virtual void _addRelation(string container, string slot,
                   string referred);

  virtual void changeRelation(string container, string slot,
                      string new_referred);
  virtual void _changeRelation(string container, string slot,
                      string new_referred);

  virtual void removeRelation(string container, string slot, string referred);

  virtual void instantiateSystem();

  virtual void printAllSystems();
  virtual void printCurrentSystem();

  virtual void printNodes();
  virtual void printClasses();
  virtual void printTypes();
  virtual void printInterfaces();
  virtual void printRelations();
  virtual void saveBN(string filename);
  virtual void saveO3PRM(string filename);
  virtual void printBN();

private:
  gum::prm::PRMFactory<double> * __prm_factory;
  gum::BayesNetFactory<double> * __bn_factory;
  string __mainSystem;
  gum::prm::IncSystem *__sys;
  gum::prm::IncSystem *__sys2;

  PRMexplorer* __prm_explorer;
  //rajouter vecteur d'evidences
  gum::HashTable<string,int> __evidences;
  gum::LazyPropagation<double>* __inf;
  gum::BayesNet<double> * __bn;

  //int == 0 si a faire une fois, 1 si plusieurs
  list<tuple<int, string, vector<string>>> __toDo;
};

#endif // INC_PRM_ENGINE_H
