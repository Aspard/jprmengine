#ifndef BASIC_PRM_ENGINE_H
#define BASIC_PRM_ENGINE_H

#include <string>

#include <agrum/BN/BayesNet.h>
#include <agrum/BN/inference/lazyPropagation.h>
#include <agrum/PRM/PRMFactory.h>
#include <agrum/PRM/elements/PRMSystem.h>

#include "IPRMengine.h"
#include "IncSystem.h"

class BasicPRMengine : public IPRMengine {
public:
  virtual void readPRM(std::string o3prmFileName,std::string system,std::string classpath);
  virtual double getPosterior(std::string instance, std::string attribute, int mod);

  virtual void addInstance(std::string clazz, std::string instance);
  virtual void addInstance(std::string clazz, std::string instance,std::string array);
  virtual void removeInstance(std::string instance);
  
  virtual void addEvidence(std::string instance, std::string attribute, int mod);
  virtual void addEvidence(const std::string& instance, const std::string& attribute, const std::vector<double>& vals);
  virtual void removeEvidence(std::string instance, std::string attribute);
  virtual void removeAllEvidences();

  virtual void addTarget(std::string instance, std::string attribute);
  virtual void removeTarget(std::string instance, std::string attribute);

  virtual double testRandomPosterior();

  virtual void addRelation(std::string container, std::string slot,
                   std::string refered);
  virtual void changeRelation(std::string container, std::string slot,
                      std::string new_refered);
  virtual void removeRelation(std::string container, std::string slot, std::string refered);
  virtual void instantiateSystem();

  virtual void toString();
private:
      //gum::prm::PRM<double> *__prm;
  //gum::prm::IncPRMFactory *__prm_factory;

  gum::prm::PRMFactory<double> * __prm_factory;
  gum::BayesNetFactory<double> * __bn_factory;
  gum::prm::IncSystem *__sys;

  //gum::prm::PRMSystem<double>* __sys;

  gum::LazyPropagation<double>* __inf;
  gum::BayesNet<double> __bn;

};

#endif // BASIC_PRM_ENGINE_H
