#include <string>
#include <vector>
#include <map>

#include <agrum/PRM/PRM.h>
#include <agrum/PRM/elements/PRMAggregate.h>
#include <agrum/PRM/elements/PRMClassElement.h>
#include <agrum/PRM/o3prm/O3prmReader.h>

#include "PRMexplorer.h"

using namespace std;

/**
* Fill an explorer from a filename and a classpath
* @param filename
* @param classpath
* @param verbose to print warnings and errors
*/
void PRMexplorer::load(string filename, string classpath, bool verbose) {
  __prm = nullptr;
  __main_system = nullptr;

  stringstream stream;

  __reader.setClassPath(classpath);

  auto nbErr = __reader.readFile(filename);

  __reader.showElegantErrorsAndWarnings(stream);
  if (nbErr > 0) {
    __reader.showErrorCounts(stream);
    GUM_ERROR(gum::FatalError, stream.str());
  }
  if (verbose) {
    cout << stream.str() << endl;
  }
  __prm = __reader.prm();
}

void PRMexplorer::addPRM(gum::prm::PRMFactory<double>* prmFactory,gum::prm::IncSystem* mainSystem) {
  __prm = prmFactory->prm();
  __main_system = mainSystem;
}

/**
* @param name The name of a possible Type<GUM_SCALAR> in this PRM.
* @return Returns true if name names a Type<GUM_SCALAR> in this PRM.
*/
bool PRMexplorer::isType(string name) {
  return __prm->isType(name);
}

/**
* @param name The name of a possible Class<GUM_SCALAR> in this PRM.
* @return Returns true if name names a Class<GUM_SCALAR> in this PRM.
*/
bool PRMexplorer::isClass(string name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }
  return __prm->isClass(name);
}

/**
* @param name The name of a possible Interface<GUM_SCALAR> in this PRM.
* @return Returns true if name names a Interface<GUM_SCALAR> in this PRM.
*/
bool PRMexplorer::isInterface(string name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  return __prm->isInterface(name);
}

gum::ArcSet PRMexplorer::getMainSystemArcs() {
  if (__main_system == nullptr){
    GUM_ERROR(gum::FatalError, "No main system.");
  }

  return __main_system->skeleton().arcs();
}


/************CLASS**************/

/**
* @return a list of class names
*/
vector<string> PRMexplorer::classes() {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> v;

  for (auto c : __prm->classes()) {
      //PyList_Append(q, PyString_FromString(c->name().c_str()));
      v.push_back(c->name().c_str());
  }

  return v;
}

/**
* @return a list of Tuplet(typename, attribute_name, depenson)
* @param class_name : the name of the class
* @param allAttributes : even automatically generated attributes
*/

vector<tuple<string,string,vector<string>>>
PRMexplorer::classAttributes(string class_name /*, bool allAttributes = false */) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<tuple<string,string,vector<string>>> v;

  auto&    c = __prm->getClass(class_name);
  gum::DAG gra = c.containerDag();

  for (auto a : c.attributes()) {
    /*if ( allAttributes ) {
    PyObject* uplet = PyTuple_New(2);
    PyTuple_SetItem(uplet, 0, PyString_FromString( c->type().name().c_str()) );
    PyTuple_SetItem(uplet, 1, PyString_FromString( c->name().c_str() ));
    PyList_Append( q, uplet);
  } else*/
    if (&(c.get(a->name())) == a) {
      // remove automatically created attributes (cast-descendant)
      string type_name = a->type().name().c_str();
      string name = a->name().c_str();

      vector<string> depenson;
      for (auto parentId : gra.parents(a->id())) {
        depenson.push_back(c.get(parentId).name().c_str());
      }

      v.push_back(make_tuple(type_name,name,depenson));
    }
  }
  return v;
}

/**
* @param class_name : the name of the class
* @param att_name : the name of the attribute contained in this class
*/
bool PRMexplorer::isAttribute(string class_name, string att_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  auto& ob = __prm->getClass(class_name).get(att_name);

  return gum::prm::PRMClassElement< double >::isAttribute(ob);
}

/**
* @return a list of Tuplet(typename,reference_name, isArray)
* @param class_name : the name of the class
*/
vector<tuple<string,string,bool>> PRMexplorer::classReferences(string class_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<tuple<string,string,bool>> q;

  for (auto r : __prm->getClass(class_name).referenceSlots()) {
    
    string type_name = r->slotType().name().c_str();
    string reference_name = r->name().c_str();

    tuple<string,string,bool> uplet = make_tuple(type_name,reference_name,r->isArray());
    q.push_back(uplet);
  }
  return q;
}


/**
* @return a list of parameters from a class
* @param class_name : the name of the class
*/
vector<string> PRMexplorer::classParameters(string class_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> q;

  for (auto c : __prm->getClass(class_name).parameters()) {
    q.push_back(c->safeName().c_str());
  }
  return q;
}

/**
* @return a list of interface which the class implemented
* @param class_name : the name of the class
*/
vector<string> PRMexplorer::classImplements(string class_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> q;

  try {
    // raise NotFound if this class doesn't implement any interface
    for (auto c : __prm->getClass(class_name).implements()) {
      q.push_back(c->name().c_str());
    }
  } catch (gum::NotFound&) { /*do nothing and return empty list*/
  }
  return q;
}

/**
  * @return a list of Tuplet(typename, name, agg_type, agg_label=None,
  * applies_label_list)
  * @param class_name : the name of the class
  */
string aggType[10] = {
  "min", "max", "count", "exists", "forall", "or", "and", "amplitude", "median","sum"};

vector<tuple<string,string,string,string,vector<string>>> PRMexplorer::classAggregates(string class_name) {

  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<tuple<string,string,string,string,vector<string>>> l;

  auto& c = __prm->getClass(class_name);

  gum::DAG gra = c.containerDag();

  for (auto a : c.aggregates()) {
    string type_name = a->type().name().c_str();
    string name = a->name().c_str();

    string aggtype = aggType[(int)a->agg_type()].c_str();

    string label = "";

    if (a->hasLabel()) {
      label = a->labelValue().c_str();
    }

    vector<string> param;

    for (auto parentId : gra.parents(a->id())) {
      param.push_back(c.get(parentId).name().c_str());
    }

    tuple<string,string,string,string,vector<string>> uplet = make_tuple(type_name,name,aggtype,label,param);
    l.push_back(uplet);
  }
  return l;
}

/**
  * @return a list of Tuplet(typename, slotname, isMultiple)
  * @param class_name : the name of the class
  */
vector<tuple<string,string,bool>> PRMexplorer::classSlotChains(string class_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<tuple<string,string,bool>> l;

  for (auto c : __prm->getClass(class_name).slotChains()) {
    string name = c->type().name().c_str();
    string slotname = c->name().c_str();

    l.push_back(make_tuple(name,slotname,c->isMultiple()));
  }
  return l;
}

/**
* @return a Tuplet(dict,string)
* @param class_name : the name of the class
*/
tuple<map<unsigned long,string>,string> PRMexplorer::classDag(string class_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  auto&     c = __prm->getClass(class_name);
  map<unsigned long,string> d;
  gum::DAG  gra = c.containerDag();
  for (auto id : gra.nodes()) {
    d.insert({(unsigned long)id,c.get(id).name().c_str()});
  }

  return make_tuple(d,gra.toDot().c_str());
}

/**
* @return a list of 3-uplets (nameofsystem,dictinary of ids  their name &
* type,list of arcs:[(tail,head),..] )
*/
vector<tuple<string,map<unsigned long,vector<string>>,vector<tuple<unsigned long,unsigned long>>>> PRMexplorer::getalltheSystems() {
  if (__prm == nullptr) {
    cout << "No loaded prm." << endl;
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  //vector<tuple<string,map<unsigned long,string>,string>> l;
  vector<tuple<string,map<unsigned long,vector<string>>,vector<tuple<unsigned long,unsigned long>>>> l;

  auto the_systems = __prm->systems();

  unsigned long var = 0;
  for (auto a_system : the_systems) {

    vector<tuple<unsigned long,unsigned long>> a;
    map<unsigned long,vector<string>>  n;

    auto graph = a_system->skeleton();

    string nameofsystem = a_system->name().c_str();

    for (auto node : graph.nodes()) {

      vector<string> tnode;
      tnode.push_back(a_system->get(node).name().c_str());
      tnode.push_back(a_system->get(node).type().name().c_str());
      
      n.insert({(unsigned long)node,tnode});
    }

    for (auto arc : graph.arcs()) {

      a.push_back(make_tuple((unsigned long)arc.tail(),(unsigned long)arc.head()));
    }

    l.push_back(make_tuple(nameofsystem,n,a));

    var = var + 1;
  }

  return l;
}
/*
tuple<string,map<unsigned long,vector<string>>,vector<tuple<unsigned long,unsigned long>>> PRMexplorer::getSystem() {
  if (__prm == nullptr) {
    cout << "No loaded prm." << endl;
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<tuple<unsigned long,unsigned long>> a;
  map<unsigned long,vector<string>>  n;

  auto graph = __sys->skeleton();

  string nameofsystem = __sys->name().c_str();

  for (auto node : graph.nodes()) {

    vector<string> tnode;
    tnode.push_back(__sys->get(node).name().c_str());
    tnode.push_back(__sys->get(node).type().name().c_str());
    
    n.insert({(unsigned long)node,tnode});
  }

  for (auto arc : graph.arcs()) {

    a.push_back(make_tuple((unsigned long)arc.tail(),(unsigned long)arc.head()));
  }

  return make_tuple(nameofsystem,n,a);
}*/

tuple<string,map<unsigned long,vector<string>>,vector<tuple<unsigned long,unsigned long>>> PRMexplorer::getTheMainSystem() {
  if (__prm == nullptr) {
    cout << "No loaded prm." << endl;
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<tuple<unsigned long,unsigned long>> a;
  map<unsigned long,vector<string>>  n;

  auto graph = __main_system->skeleton();

  string nameofsystem = __main_system->name().c_str();

  for (auto node : graph.nodes()) {

    vector<string> tnode;
    tnode.push_back(__main_system->get(node).name().c_str());
    tnode.push_back(__main_system->get(node).type().name().c_str());
    
    n.insert({(unsigned long)node,tnode});
  }

  for (auto arc : graph.arcs()) {

    a.push_back(make_tuple((unsigned long)arc.tail(),(unsigned long)arc.head()));
  }

  return make_tuple(nameofsystem,n,a);
}

/**
* @return the name of the super class
* @param class_name : the name of the class
*/
string PRMexplorer::getSuperClass(string class_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  auto& c = __prm->getClass(class_name);
  try {
    // raise NotFound if this class haven't super
    return c.super().name().c_str();
  } catch (gum::NotFound&) {
    return "";
  }
}

/**
* @return the list of subclass name
* @param class_name : the name of the class
*/
vector<string> PRMexplorer::getDirectSubClass(string class_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> l;

  auto&     exten = __prm->getClass(class_name).extensions();
  for (auto c : exten) {
    l.push_back(c->name().c_str());
  }
  return l;
}

/**
* @return the potential of an attribute in a class
* @param classname : the name of the class   *
* @param attribute : the name of the attribute
*/
const gum::Potential< double >& PRMexplorer::cpf(string class_name,
                                    string attribute) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }
  return __prm->getClass(class_name).get(attribute).cpf();
}

/************TYPE**************/

/**
* @return the list of type names
*/
vector<string> PRMexplorer::types() {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> l;

  for (auto c : __prm->types())
    l.push_back(c->name().c_str());
  return l;
}

/**
* @return the name of super type
* @param type_name : the name of the type
*/
string PRMexplorer::getSuperType(string type_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  auto& c = __prm->type(type_name);
  if (c.isSubType()) {
    return c.superType()->name().c_str();
  } else {
    return "";
  }
}

/**
* @return the list of subtype name
* @param type_name : the name of the type
*/
vector<string> PRMexplorer::getDirectSubTypes(string type_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> l;
  auto&     selected = __prm->type(type_name);
  for (auto c : __prm->types()) {
    if (c->isSubType()) {
      if (c->superType().name() == selected.name()) {
        l.push_back(c->name().c_str());
      }
    }
  }
  return l;
}

/**
* @return the list of labels name
* @param type_name : the name of the type
*/
vector<string> PRMexplorer::getLabels(string type_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> l;
  auto&     selected = __prm->type(type_name);
  for (auto lab : selected->labels()) {
    l.push_back(lab.c_str());
  }
  return l;
}

/**
* @return the dictionaty which the value is the super type of the key
* @param type_name : the name of the type
*/
pair<map<string,string>,bool> PRMexplorer::getLabelMap(string type_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  map<string,string> d ;
  auto&     selected = __prm->type(type_name);
  if (!selected.isSubType()) {
    return make_pair(d,false);
  }
  auto  typeLabelVector = selected.variable().labels();
  auto  superTypeLabelVector = selected.superType().variable().labels();
  auto& labelMapTypeToSuperType = selected.label_map();
  for (unsigned i = 0; i != labelMapTypeToSuperType.size(); i++) {
    d.insert({typeLabelVector[i].c_str(),superTypeLabelVector[labelMapTypeToSuperType[i]].c_str()});
  }
  return make_pair(d,true);
}

/************INTERFACE**************/

/**
* @return the list of interface names
*/
vector<string> PRMexplorer::interfaces() {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> l;

  for (auto c : __prm->interfaces())
    l.push_back(c->name().c_str());
  return l;
}

/**
* @return a list of attribute names from a interface
* @param interface_name : the name of the interface
* @param allAttributes : even automatically generated attributes
*/
vector<tuple<string,string>> PRMexplorer::interAttributes(string interface_name, bool allAttributes) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<tuple<string,string>> q;

  for (auto c : __prm->getInterface(interface_name).attributes())
    if (allAttributes) {
      q.push_back(make_tuple(c->type().name().c_str(),c->name().c_str()));
    } else if (&(__prm->getInterface(interface_name).get(c->name())) == c) {
      q.push_back(make_tuple(c->type().name().c_str(),c->name().c_str()));
    }

  return q;
}

/**
* @return a list of attribute names from a interface
* @param interface_name : the name of the interface
* @param allAttributes : even automatically generated attributes
*/
vector<tuple<string,string,bool>> PRMexplorer::interReferences(string interface_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<tuple<string,string,bool>> q;

  for (auto r : __prm->getInterface(interface_name).referenceSlots()) {
    q.push_back(make_tuple(r->slotType().name().c_str(),r->name().c_str(),r->isArray()));
  }
  return q;
}

/**
* @return the name of super interface
* @param interface_name : the name of the interface
*/
string PRMexplorer::getSuperInterface(string interface_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  auto& c = __prm->getInterface(interface_name);
  try {
    // raise NotFound if this interface haven't super
    return c.super().name().c_str();
  } catch (gum::NotFound&) {
    return "";
  }
}

/**
* @return the list of subinterface name
* @param interface_name : the name of the interface
*/
vector<string> PRMexplorer::getDirectSubInterfaces(string interface_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> l;
  auto&     selected = __prm->getInterface(interface_name);
  for (auto c : __prm->interfaces()) {
    try {
      // raise NotFound if this interface haven't super
      if (c->super().name() == selected.name()) {
        l.push_back(c->name().c_str());
      }
    } catch (gum::NotFound&) { /*do nothing, skip this iteration*/
    }
  }
  return l;
}

/**
* @return the list of class name how implement this interface
* @param interface_name : the name of the interface
*/
vector<string> PRMexplorer::getImplementations(string interface_name) {
  if (__prm == nullptr) {
    GUM_ERROR(gum::FatalError, "No loaded prm.");
  }

  vector<string> l;
  auto&     selected = __prm->getInterface(interface_name);
  for (auto c : selected.implementations()) {
    l.push_back(c->name().c_str());
  }
  return l;
}