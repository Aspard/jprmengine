/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package org.agrum.prmEngine;

public class IncPRMengine {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected IncPRMengine(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(IncPRMengine obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        PRMengineJNI.delete_IncPRMengine(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    PRMengineJNI.IncPRMengine_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    PRMengineJNI.IncPRMengine_change_ownership(this, swigCPtr, true);
  }

  public void readPRM(String o3prmFileName, String system, String classpath) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_readPRM(swigCPtr, this, o3prmFileName, system, classpath); else PRMengineJNI.IncPRMengine_readPRMSwigExplicitIncPRMengine(swigCPtr, this, o3prmFileName, system, classpath);
  }

  public double getPosterior(String instance, String attribute, int mod) {
    return (getClass() == IncPRMengine.class) ? PRMengineJNI.IncPRMengine_getPosterior(swigCPtr, this, instance, attribute, mod) : PRMengineJNI.IncPRMengine_getPosteriorSwigExplicitIncPRMengine(swigCPtr, this, instance, attribute, mod);
  }

  public void addInstance(String clazz, String instance) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_addInstance__SWIG_0(swigCPtr, this, clazz, instance); else PRMengineJNI.IncPRMengine_addInstanceSwigExplicitIncPRMengine__SWIG_0(swigCPtr, this, clazz, instance);
  }

  public void addInstance(String clazz, String instance, String array) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_addInstance__SWIG_1(swigCPtr, this, clazz, instance, array); else PRMengineJNI.IncPRMengine_addInstanceSwigExplicitIncPRMengine__SWIG_1(swigCPtr, this, clazz, instance, array);
  }

  public void addInstanceOnce(String clazz, String instance) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_addInstanceOnce__SWIG_0(swigCPtr, this, clazz, instance); else PRMengineJNI.IncPRMengine_addInstanceOnceSwigExplicitIncPRMengine__SWIG_0(swigCPtr, this, clazz, instance);
  }

  public void addInstanceOnce(String clazz, String instance, String array) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_addInstanceOnce__SWIG_1(swigCPtr, this, clazz, instance, array); else PRMengineJNI.IncPRMengine_addInstanceOnceSwigExplicitIncPRMengine__SWIG_1(swigCPtr, this, clazz, instance, array);
  }

  public void _addInstance(String clazz, String instance) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine__addInstance__SWIG_0(swigCPtr, this, clazz, instance); else PRMengineJNI.IncPRMengine__addInstanceSwigExplicitIncPRMengine__SWIG_0(swigCPtr, this, clazz, instance);
  }

  public void _addInstance(String clazz, String instance, String array) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine__addInstance__SWIG_1(swigCPtr, this, clazz, instance, array); else PRMengineJNI.IncPRMengine__addInstanceSwigExplicitIncPRMengine__SWIG_1(swigCPtr, this, clazz, instance, array);
  }

  public void removeInstanceOnce(String clazz) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_removeInstanceOnce(swigCPtr, this, clazz); else PRMengineJNI.IncPRMengine_removeInstanceOnceSwigExplicitIncPRMengine(swigCPtr, this, clazz);
  }

  public void removeInstance(String instance) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_removeInstance(swigCPtr, this, instance); else PRMengineJNI.IncPRMengine_removeInstanceSwigExplicitIncPRMengine(swigCPtr, this, instance);
  }

  public void _removeInstance(String instance) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine__removeInstance(swigCPtr, this, instance); else PRMengineJNI.IncPRMengine__removeInstanceSwigExplicitIncPRMengine(swigCPtr, this, instance);
  }

  public void addEvidence(String instance, String attribute, int mod) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_addEvidence__SWIG_0(swigCPtr, this, instance, attribute, mod); else PRMengineJNI.IncPRMengine_addEvidenceSwigExplicitIncPRMengine__SWIG_0(swigCPtr, this, instance, attribute, mod);
  }

  public void addEvidence(String instance, String attribute, DoubleVector vals) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_addEvidence__SWIG_1(swigCPtr, this, instance, attribute, DoubleVector.getCPtr(vals), vals); else PRMengineJNI.IncPRMengine_addEvidenceSwigExplicitIncPRMengine__SWIG_1(swigCPtr, this, instance, attribute, DoubleVector.getCPtr(vals), vals);
  }

  public void removeEvidence(String instance, String attribute) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_removeEvidence(swigCPtr, this, instance, attribute); else PRMengineJNI.IncPRMengine_removeEvidenceSwigExplicitIncPRMengine(swigCPtr, this, instance, attribute);
  }

  public void removeAllEvidences() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_removeAllEvidences(swigCPtr, this); else PRMengineJNI.IncPRMengine_removeAllEvidencesSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void makeInference() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_makeInference(swigCPtr, this); else PRMengineJNI.IncPRMengine_makeInferenceSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void addTarget(String instance, String attribute) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_addTarget(swigCPtr, this, instance, attribute); else PRMengineJNI.IncPRMengine_addTargetSwigExplicitIncPRMengine(swigCPtr, this, instance, attribute);
  }

  public void removeTarget(String instance, String attribute) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_removeTarget(swigCPtr, this, instance, attribute); else PRMengineJNI.IncPRMengine_removeTargetSwigExplicitIncPRMengine(swigCPtr, this, instance, attribute);
  }

  public double testRandomPosterior() {
    return (getClass() == IncPRMengine.class) ? PRMengineJNI.IncPRMengine_testRandomPosterior(swigCPtr, this) : PRMengineJNI.IncPRMengine_testRandomPosteriorSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void addRelationOnce(String container, String slot, String referred) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_addRelationOnce(swigCPtr, this, container, slot, referred); else PRMengineJNI.IncPRMengine_addRelationOnceSwigExplicitIncPRMengine(swigCPtr, this, container, slot, referred);
  }

  public void addRelation(String container, String slot, String referred) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_addRelation(swigCPtr, this, container, slot, referred); else PRMengineJNI.IncPRMengine_addRelationSwigExplicitIncPRMengine(swigCPtr, this, container, slot, referred);
  }

  public void _addRelation(String container, String slot, String referred) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine__addRelation(swigCPtr, this, container, slot, referred); else PRMengineJNI.IncPRMengine__addRelationSwigExplicitIncPRMengine(swigCPtr, this, container, slot, referred);
  }

  public void changeRelation(String container, String slot, String new_referred) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_changeRelation(swigCPtr, this, container, slot, new_referred); else PRMengineJNI.IncPRMengine_changeRelationSwigExplicitIncPRMengine(swigCPtr, this, container, slot, new_referred);
  }

  public void _changeRelation(String container, String slot, String new_referred) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine__changeRelation(swigCPtr, this, container, slot, new_referred); else PRMengineJNI.IncPRMengine__changeRelationSwigExplicitIncPRMengine(swigCPtr, this, container, slot, new_referred);
  }

  public void removeRelation(String container, String slot, String referred) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_removeRelation(swigCPtr, this, container, slot, referred); else PRMengineJNI.IncPRMengine_removeRelationSwigExplicitIncPRMengine(swigCPtr, this, container, slot, referred);
  }

  public void instantiateSystem() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_instantiateSystem(swigCPtr, this); else PRMengineJNI.IncPRMengine_instantiateSystemSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void printAllSystems() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_printAllSystems(swigCPtr, this); else PRMengineJNI.IncPRMengine_printAllSystemsSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void printCurrentSystem() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_printCurrentSystem(swigCPtr, this); else PRMengineJNI.IncPRMengine_printCurrentSystemSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void printNodes() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_printNodes(swigCPtr, this); else PRMengineJNI.IncPRMengine_printNodesSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void printClasses() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_printClasses(swigCPtr, this); else PRMengineJNI.IncPRMengine_printClassesSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void printTypes() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_printTypes(swigCPtr, this); else PRMengineJNI.IncPRMengine_printTypesSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void printInterfaces() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_printInterfaces(swigCPtr, this); else PRMengineJNI.IncPRMengine_printInterfacesSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void printRelations() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_printRelations(swigCPtr, this); else PRMengineJNI.IncPRMengine_printRelationsSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public void saveBN(String filename) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_saveBN(swigCPtr, this, filename); else PRMengineJNI.IncPRMengine_saveBNSwigExplicitIncPRMengine(swigCPtr, this, filename);
  }

  public void saveO3PRM(String filename) {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_saveO3PRM(swigCPtr, this, filename); else PRMengineJNI.IncPRMengine_saveO3PRMSwigExplicitIncPRMengine(swigCPtr, this, filename);
  }

  public void printBN() {
    if (getClass() == IncPRMengine.class) PRMengineJNI.IncPRMengine_printBN(swigCPtr, this); else PRMengineJNI.IncPRMengine_printBNSwigExplicitIncPRMengine(swigCPtr, this);
  }

  public IncPRMengine() {
    this(PRMengineJNI.new_IncPRMengine(), true);
    PRMengineJNI.IncPRMengine_director_connect(this, swigCPtr, true, true);
  }

}
