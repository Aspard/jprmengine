#define GUM_TRACE_ON

#include <iostream>
#include <assert.h>     /* assert */



#include <agrum/config.h>
#include <agrum/BN/inference/lazyPropagation.h>
#include <agrum/PRM/inference/structuredInference.h>

#include <agrum/PRM/o3prm/O3prmReader.h>
//#include <agrum/BN/io/BIF/BIFReader.h>

#include "IncPRMFactory.h"
#include "IncSystem.h"
#include "IncrementalLazyInference.h"

class Tests {
private:
 typedef gum::prm::Class<double> Class;
 typedef gum::prm::PRM<double> PRM;
 typedef gum::prm::System<double> System;
 typedef gum::prm::Instance<double> Instance;

 typedef gum::prm::IncPRMFactory IncPRMFactory;
 typedef gum::prm::IncSystem IncSystem;
typedef gum::prm::Attribute<double> Attribute;




     IncPRMFactory *prm_factory;

     IncSystem *sys;
    PRM *prm;
    gum::BayesNet<double> *bn;
    std::string syst_name = "sys";
    std::string dir = "resources/";
    std::string file_name =dir+"printers_system.o3prm";
    //Timer timer;
    //double LI_time = 0.0;

public:
    Tests() {
        gum::prm::o3prm::O3prmReader<double> reader;
        reader.setClassPath(dir);
        reader.readFile(file_name);


        reader.showElegantErrorsAndWarnings();
        prm_factory = new  IncPRMFactory(reader.prm());
        prm = prm_factory->prm();
        sys=new  IncSystem((prm->system( syst_name )));
    }

    const Instance&pickInstance(const System &sys) {
        auto pick = std::rand() % sys.size();

        for (auto inst : sys) {
            if (pick == 0)
                return *(inst.second);
            pick--;
        }
    }
    void printProba( const gum::Potential<double>& p ) {
        gum::Instantiation inst(p);
        for (inst.setFirst(); !inst.end(); ++inst) {
            std::cerr << inst<<" : " <<p[inst] << std::endl;
        }
        std::cerr << std::endl;
    }
    const Attribute &pickAttribute(const Instance& instance) {
        auto pick = std::rand() % instance.size();

        for (auto attr : instance) {
            GUM_TRACE_VAR((attr.second)->name());
            if (pick == 0)
                return *(attr.second);
            pick--;
        }
    }

    void del() {
        delete prm;
        delete prm_factory;
    }

    void test_addObject() {
        std::cout << "-------------------------------------" << std::endl;
        std::cout << "             test_addObject          " << std::endl;
        std::cout << "-------------------------------------" << std::endl;

        unsigned int befre_size(sys->size());

        sys->addObject( prm->getClass("PowerSupply"), "newPow");
        std::cout << "addObject(" + syst_name + ",\"PowerSupply\",\"newPow\")"<< std::endl;



        sys->addObject( prm->getClass("Computer"), "newC");
        std::cout << "addObject(" + syst_name + ",\"Computer\",\"newC\")" << std::endl;

        sys->addObject( prm->getClass("Printer"), "newPrinter");
        std::cout << "addObject(" + syst_name + ",\"Printer\",\"newPrinter\")" << std::endl;

        sys->addObject( prm->getClass("Room") , "newR");
        std::cout << "addObject(" + syst_name + ",\"Room\",\"newR\")" << std::endl;

        unsigned int after_size(sys->size());


        if (befre_size != after_size - 4) {
            std::cout << "test size NOT OK !!! " << befre_size << " " << after_size << std::endl;
        }
        if (not(sys->exists("newPow") && sys->exists("newR")&& sys->exists("newPrinter"))) {
            std::cout << "test exist NOT OK !!! " << std::endl;
        }

        if (not(sys->isInstance("newPow") && sys->isInstance("newR")&& sys->isInstance("newPrinter"))) {
            std::cout << "test isInstance NOT OK !!! " << std::endl;
        }
        for (const auto &i : *sys) {
            std::cout << i.second->name() << std::endl;
        }

    }
    void test_adjustSystem() {
        std::cout << "-------------------------------------" << std::endl;
        std::cout << "             adjustSystem             " << std::endl;
        std::cout << "-------------------------------------" << std::endl;
        sys->adjust();
    }
    void test_setReference() {

        std::cout << "-------------------------------------" << std::endl;
        std::cout << "          test_setReference          " << std::endl;
        std::cout << "-------------------------------------" << std::endl;

        sys->setReferenceSlot("newR","power","newPow");
        std::cout<<"setReferenceSlot(\"newR\",\"power\",\"newPow\");"<<std::endl;

        sys->setReferenceSlot("newC","room","newR");
        std:: cout  << "setReferenceSlot(\"newC\",\"room\",\"newR\");"<<std::endl;

        sys->setReferenceSlot("newC","printers","printers");
        std::cout << "setReferenceSlot(\"newC\",\"printers\",\"printers\");"<<std::endl;

        sys->setReferenceSlot("newPrinter","room","newR");
        std::cout << "setReferenceSlot(\"newPrinter\",\"room\",\"newR\");"<<std::endl;

        std:: cout << "isInstance(\"newC\"): "<<sys->isInstance("newC")<<std::endl
                   <<"exists(\"newC\")     : "<<sys->exists("newC")<<std::endl;
    }
    void test_removeObject() {
        std::cout << "-------------------------------------" << std::endl;
        std::cout << "             test_removeObject       " << std::endl;
        std::cout << "-------------------------------------" << std::endl;

        std::cout<<"Initial IncSystem"<<std::endl;
        std::cout<<"----------"<<std::endl;
        for (const auto &i:*sys ) {
            std::cout<< i.second->name()<<std::endl;
        }

        unsigned  int before_size(sys->size());
        std::string name="newC";
        //name="newC"; // now it's OK because newC is not referred
        GUM_TRACE_VAR(sys->exists(name));
        GUM_TRACE_VAR(sys->isInstance(name));

        // use sys et pas prm->system(system_name) qui renvoie au system initial
        sys->removeObject(name);

        unsigned  int after_size(sys->size());
        if(before_size!= after_size + 1 ) {
            std:: cout<<"test size NOT OK !!! "<<before_size<<" "<< after_size<<std::endl;
        }
        if(sys->exists(name) ) {
            std:: cout<<"test exist NOT OK !!! "<<std::endl;
        }


        if(sys->isInstance(name)) {
            std:: cout<<"test isInstance NOT OK !!! "<<std::endl;
        }
        std::cout<< "size before: "+before_size<<
                 "size after: " << sys->size()<<std::endl;

        std:: cout<<"IncSystem after deleting: " + name<<std::endl;
        std:: cout<<"----------"<<std::endl;
        for (const auto &i:*sys ) {
            std::cout<< i.second->name()<<std::endl;
        }

    }

    void test_SIJTI() {

        std::cout << "-------------------------------------" << std::endl;
        std::cout << "             test_SIJTI              " << std::endl;
        std::cout << "-------------------------------------" << std::endl;
        try {
            // SIJTI incInf(*prm, *bn);

            //std::cout << "first call of LI" << std::endl;
            // incInf.lazyInference();
            //std::cout << "second call of LI" << std::endl;
            // incInf.lazyInference();

            // nodeSet construction
            /* NodeId powState_id = prm->getClass("PowerSupply").get("powState").id();
             NodeSet pow_targetable {};
             NodeSet pow_observables {powState_id};

             NodeId can_print_id = prm->getClass("Computer").get("can_print").id();
             NodeSet computer_targetable {can_print_id};
             NodeSet computer_observables {};

             NodeId hasPaper_id = prm->getClass("Printer").get("hasPaper").id();
             NodeId equipState_id = prm->getClass("Printer").get("equipState").id();
             NodeSet pr_targetable {hasPaper_id};
             NodeSet pr_observables {equipState_id};

             HashTable<const Class<double> *, std::pair<NodeSet, NodeSet>> classes2nodes;
             std::pair<NodeSet, NodeSet> pow_nodes =
                 std::make_pair(pow_targetable, pow_observables);
             std::pair<NodeSet, NodeSet> c_nodes =
                 std::make_pair(computer_targetable, computer_observables);
             std::pair<NodeSet, NodeSet> pr_nodes =
                 std::make_pair(pr_targetable, pr_observables);

             classes2nodes.insert(&prm->getClass("PowerSupply"), pow_nodes);
             classes2nodes.insert(&prm->getClass("Computer"), c_nodes);
             classes2nodes.insert(&prm->getClass("Printer"), pr_nodes);

             prm_factory->setClass2nodes_types(classes2nodes);

             GUM_TRACE("============test classInfo init===================")*/

            /* for(HashTable<const Class<double>*, std::pair<NodeSet,NodeSet>>::const_iterator
             iter =prm_factory->classes2nodes_types().begin();
                     iter!=prm_factory->classes2nodes_types().end(); ++iter) {
                   //std::cout <<  iter.val().first<< *iter.key() <<std::endl;// targetable

             nodeSet for every class
                   std::cout<< classes2nodes[iter.key()].first <<std::endl;
                   std::cout<< iter.val().first<<std::endl;
             }*/


            /* prm_factory->build_C_off();
             for (auto cl= prm_factory->C_off().begin(); cl!=prm_factory->C_off().end(); ++cl) {
                 GUM_TRACE_VAR(cl.val()->LMN())
                 std::cout<<"barren_inners() for class " << (*cl.key()).name() << " are: "<<  cl.val()->barren_inners()<<std::endl;
                 for ( auto n= cl.val()->barren_inners().begin(); n!= cl.val()->barren_inners().end(); ++n) {
                     auto iter=cl.val()->set_of_potentials().begin();
                     while (iter!=cl.val()->set_of_potentials().end()) {
                         if((*iter)->contains(((*cl.key()).get(*n)).type().variable()) ) {
                             std::cout<< "Problem: the node "<< *n<<"="<< (*cl.key()).get(*n).name()<<"still exists after elimination !!!"<<std::endl;
                         } else {
                             ++iter;
                         }
                     }
                 }
             }*/

        } catch (gum::Exception &e) {
            throw;
        }
    }

   /* void test_infer() {
        std::cout << "-------------------------------------" << std::endl;
        std::cout << "             test_infer              " << std::endl;
        std::cout << "-------------------------------------" << std::endl;

        try {
            gum::prm::StructuredInference<double> inf(*prm, *sys);
            inf.setPatternMining(false);

            const Instance&i = pickInstance(*sys);

            const Attribute &a = pickAttribute(i);


            gum::prm::PRMInference<double>::Chain chain = std::make_pair(&i, &a);


            gum::Potential<double> pot;

            inf.marginal(chain, pot);



            std::cout << inf.info();


            double sum = 0.0;
            gum::Instantiation inst(pot);

            for (inst.setFirst(); not inst.end(); inst.inc())
                sum += pot.get(inst);

            std::cout << "sum of marginal: " + std::to_string(sum) << std::endl;
            std::cout << i.hasRefAttr(a.id()) << std::endl;

        } catch (gum::Exception &e) {
            throw;
        }
    }*/

};

void testPot() {
    gum::Potential<double> p1( new gum::MultiDimArray<double>() );
    gum::Potential<double> p2( new gum::MultiDimArray<double>() );

    gum::LabelizedVariable a( "a", "first var", 2 ),
        b( "b", "second var", 2 ), c( "c", "third var", 2 );
    p1 << a << b ;
    p2 <<c;

    p1.fillWith( std::vector<double> {0.1, 0.9, 0.8, 0.2} );
    p2.fillWith( std::vector<double> {0.3, 0.7} );
}



int main(void) {
    testPot();
    gum::Potential<double> p;

    Tests tst;



    /// in the order !!

    /*tst.test_addObject();
    tst.test_setReference();
    tst.test_adjustSystem();

    tst.test_removeObject();
    tst.test_infer();
    //tst.test_SIJTI();*/
return 0;
}
