#define GUM_TRACE_ON
#include <iostream>
#include <fstream>
#include <assert.h>     /* assert */
#include <math.h>

#include <agrum/config.h>
#include <agrum/BN/io/BIF/BIFReader.h>
#include <agrum/core/timer.h>
//#include <ctime>
//#include <stdlib.h>
#include <agrum/multidim/multiDimArray.h>
#include <agrum/variables/labelizedVariable.h>

#include <limits>
#include<agrum/BN/inference/lazyPropagation.h>
#include<LazyIncrementalInference.h>

class test_times {
public:
    gum::NodeSet sample(gum::NodeSet initial_set,double percentage) {
        gum::Size  initial_set_size = initial_set.size();
        gum::Size extracted_size = round(percentage*initial_set_size);
        gum::NodeSet my_sample;

        while (my_sample.size()!=extracted_size) {
            my_sample.insert(*(initial_set.begin()+(rand() % initial_set_size)));
        }
        return my_sample;
    }
    void set_random_evs(gum::prm::LazyIncrementalInference* incInf,double ev_percent) {
        auto domainSizes = incInf->domainSizes();
        auto bn_nodes = incInf->BayesNet().dag().asNodeSet();
        for(auto ev: sample(bn_nodes,ev_percent)) {
            incInf->addEvidence(ev,gum::randomDistribution<double>(domainSizes[ev]));
            incInf->evidence()[ev]->translate(1).normalizeAsCPT();

        }



    }

// assume refresh processes the same bn and evidence/targets percentages<=80
// refresh inference engine target+evidence with h_ratio and s_ratio;
// h_ratio : ratio corresponding to changes in JT structure: replace hard evidence
//      with others hard, targets with new ones and soft evidence with new ones that do not belong to the JT.
// s_ratio corresponds to change value of evidences already appear in the JT

    void refresh(gum::prm::LazyIncrementalInference* incInf,
                 double h_ratio,
                 double s_ratio) {


        // pruned nodes correspond to set of nodes that are pruned from
        // the bn during last inference
        gum::NodeSet bn_nodes = incInf->BayesNet().dag().asNodeSet(),
                     pruned_ids = incInf->prunedNodes(),evs, not_evidences_ids;

        for (auto e : incInf->evidence()) evs.insert(e.first);
        not_evidences_ids=bn_nodes-evs;


        auto domainSizes = incInf->domainSizes();

        // evidences belonging to __graph to change
//        for (auto soft_id: sample(incInf->softEvidenceNodes(),s_ratio)) {
//            incInf->chgEvidence(soft_id,gum::randomDistribution<double>(domainSizes[soft_id]));
//        }

        for (auto soft_id: sample(incInf->softEvidenceNodes(),s_ratio)) {
            auto pot = incInf->evidence()[soft_id];
            pot->translate(2).normalizeAsCPT();
            incInf->chgEvidence(*pot);
        }


        // percentage of targets, evidence<=70 and ratios<=.4
        auto ids_to_replace = sample(evs+incInf->targets(),h_ratio);
        for(auto id: ids_to_replace) {
            // if id has evidence (hard or soft) : take another non evidence node and set it to hard
            if(incInf->hasEvidence(id)) {
                try {
                    incInf->eraseEvidence(id);
                    auto newEv= *( not_evidences_ids.begin()+  (rand()%((int)not_evidences_ids.size())));
                    not_evidences_ids.erase(newEv);
                    if(pruned_ids.contains(newEv)) {
                        incInf->addEvidence(newEv,gum::randomDistribution<double>(domainSizes[newEv]));
                        incInf->evidence()[newEv]->translate(2).normalizeAsCPT();
                    } else {
                        incInf->addEvidence(newEv,1);
                    }
                } catch(gum::Exception&e) {
                    GUM_SHOWERROR(e);
                }
            }
            // else,  id is target:
            //if pruned nodes is not empty take a node from it and set it to target
            else {
                if (!pruned_ids.empty()) {
                    try {
                        incInf->eraseTarget(id);
                        auto newTarg=*( pruned_ids.begin()+  (rand()%((int)pruned_ids.size())));
                        incInf->addTarget(newTarg);
                        pruned_ids.erase(newTarg);
                    } catch(gum::Exception&e) {
                        GUM_SHOWERROR(e)
                    }
                }
                // else (set id to hard and erase an evidence from evidences to conserve %)
                else {
                    try {
                        incInf->addEvidence(id,1);
                        not_evidences_ids.erase(id);
                        auto ev=(*(incInf->evidence().begin()+(rand()%(int)incInf->nbrEvidence()))).first;
                        incInf->eraseEvidence(ev);
                        ids_to_replace.erase(ev);

                    } catch(gum::Exception&e) {
                        GUM_SHOWERROR(e)
                    }
                }
            }


        }
    }
    void compare_times() {
        /**
        on commence par choisir al�atoirement un ensemble de targets et evidence,
        ensuite on fait une premi�re inference,
        ensuite on fait 20 infercene correspondant � une simulation telle qu'on
        rafrachit les targets et les evidences d'une inference � l'autre avec
        h_ratio et s_ratio.
        on rep�te ce process 20 fois, ce qui corespond au nombre d'iterations
        pour faire une moyenne
        on reitere la meme chose pour les pourcentage suivants (targets et evidence)

        log file name is bn_name-h_ratio-s_ratio.txt  and contains the following
        repeated block:


        <---------------  2*nb_infer_per_simulation ---------->

        nb_iter_per_percentage, [targets_percentages], 0, ..., 0
        targ_percent,            ev_percent,           0, ..., 0
        t_inc, t_lp, t_inc, t_lp                       .........
        ..
        ..  \times nb_iter_per_percentage
        ..
        t_inc, t_lp, t_inc, t_lp                       .........
        targ_percent,            ev_percent,           0, ..., 0
        t_inc, t_lp, t_inc, t_lp                       .........
        ..
        ..  \times nb_iter_per_percentage
        ..
        t_inc, t_lp, t_inc, t_lp                       .........
        .
        .


        */
        gum::initRandom( GUM_RANDOMSEED );
        srand((int)(time(0)));
        gum::Timer timer;

        gum::prm::LazyIncrementalInference* incInf;
        gum::LazyPropagation<double>* lpInf;
//
//        std::vector<double> hard_refresh {0,.1,.15,.2,.25,.3,.35},
//          soft_refresh {.1,.15,.2,.25,.3,.35,.4},
//             targets_percentages {.03,.06,.11,.17,.22,.28,.33,.39,.44,.5},
// 				 {.03,.06,.11,.17,.22,.28,.33,.39,.44,.5};

        int nb_infer_per_simulation(20),nb_iter_per_percentage(20);
        std::vector<double>
        hard_refresh {.1},
        soft_refresh {.1},
        targets_percentages  {.5},
        evidences_percentages{.03,.06,.11,.17,.22,.28,.33,.39,.44,.5};
        std::string ressources("resources/"),logs("resources/incremental_log/");
        std::vector<std::string> bayesian_networks {
            /* "bn201511141324-30-0","bn201511141324-30-1","bn201511141324-30-2",
            "bn201511141324-45-0","bn201511141324-45-1","bn201511141324-45-2","bn201511141325-55-0",
            "bn201511141325-55-1", "bn201511141325-55-2","bn201511141326-65-0","bn201511141326-65-1",
            "bn201511141326-65-2","bn201511141326-80-0","bn201511141326-80-1","bn201511141326-80-2",*/
//					"bn201511141327-95-0","bn201511141327-95-1","bn201511141327-95-2",
//                    "bn201511141328-120-0","bn201511141328-120-1","bn201511141328-120-2",
//                    "bn201511141329-300-0","bn201511141329-300-1","bn201511141329-300-2",
//					"bn201511141333-600-0","bn201511141333-600-1","bn201511141333-600-2",
//					"bn201612230013-800-0",	"bn201612230013-800-1",	"bn201612230013-800-2",
//						"alarm",
//                      "hailfinder",
//						"insurance",
//						"water",
//						"mildew",

						"hepar2",
//						"win95",

//						"diabetes",
//						"pigs",
//						"pathfinder",

        };
        for(auto bn_name:bayesian_networks) {
            GUM_TRACE(" processing ..."+ bn_name)
            gum::BayesNet<double>* bn = new gum::BayesNet<double>();
            gum::BIFReader<double> reader( bn,ressources+bn_name+".bif" );
            std::cout << reader.proceed() << std::endl;
            assert( reader.errors()==(gum::Size)0 );
            std::ofstream log_file;
            auto bn_nodes=bn->dag().asNodeSet();
            for(auto h_ratio:hard_refresh) {
                for(auto s_ratio:soft_refresh) {
                    std::stringstream name;
                    name <<logs<<bn_name<<"-"<<h_ratio<<"-"<<s_ratio<<"delete_me.txt";
                    log_file.open (name.str(),std::fstream::app);
                    auto sep = "";

                    log_file<<nb_iter_per_percentage;
                    for (auto i : targets_percentages) log_file<<", "<<i;
                    for (auto i=0; i<2*nb_infer_per_simulation-(int)targets_percentages.size()-1; ++i) log_file<<", "<<0;
                    log_file<<"\n";
                    for (auto targ_percent: targets_percentages) {
                        for(auto ev_percent: evidences_percentages) {
                            // open a file for logging
                            log_file<<std::setprecision (7) <<targ_percent<<", "<<ev_percent;
                            for (auto i=0; i<2*nb_infer_per_simulation-2; ++i) log_file<<","<<0;
                            log_file<<"\n";
                            for(auto iter_per_percentage=0; iter_per_percentage<nb_iter_per_percentage; ++iter_per_percentage) {
                                incInf = new gum::prm::LazyIncrementalInference(bn);
                                lpInf=new gum::LazyPropagation<double> (bn,
                                                                        gum::RelevantPotentialsFinderType::DSEP_BAYESBALL_POTENTIALS,
                                                                        gum::FindBarrenNodesType::FIND_BARREN_NODES,
                                                                        false);

                                //set random targets
                                lpInf->eraseAllTargets();
                                for (auto targ:sample(bn_nodes,targ_percent)) {
                                    lpInf->addTarget(targ);
                                    incInf->addTarget(targ);
                                }
                                set_random_evs(incInf,ev_percent);
                                for (auto ev: incInf->evidence())lpInf->addEvidence(*ev.second);

                                // make the first inference
                                incInf->makeInference();
                                lpInf->makeInference();
                                for(auto targ: incInf->targets()) {
                                    incInf->posterior(targ);
                                    lpInf->posterior(targ);
                                }
                                delete lpInf;

                                sep = "";
                                double t=0.;
                                for(auto infer_i=0; infer_i<nb_infer_per_simulation; ++infer_i) {
                                    // refresh inference engine from infer_i to infer_i+1
                                    // make incremental inference and log time

                                    refresh(incInf,h_ratio,s_ratio);
                                    //for (auto ev : incInf->evidence())GUM_TRACE_VAR(*(ev.second))

                                    try {
                                        timer.reset();
                                        incInf->makeInference();
                                        for(auto targ:incInf->targets()) incInf->posterior(targ);
                                        t=timer.step();
                                        log_file<< sep << t;
                                    } catch(gum::Exception& e) {
                                        GUM_SHOWERROR(e)
                                    }

                                    timer.reset();
                                    lpInf=new gum::LazyPropagation<double>(bn,
                                                                           gum::RelevantPotentialsFinderType::DSEP_BAYESBALL_POTENTIALS,
                                                                           gum::FindBarrenNodesType::FIND_BARREN_NODES,
                                                                           false);
                                    lpInf->eraseAllTargets();
                                    for(auto targ:incInf->targets())lpInf->addTarget(targ);
                                    for (auto ev: incInf->evidence())lpInf->addEvidence(*(ev.second));
                                    lpInf->makeInference();
                                    for(auto targ:incInf->targets()) lpInf->posterior(targ);
                                    t=timer.step();
                                    log_file<<", " <<t;
                                    sep = ", ";
                                    delete lpInf;
                                }
                                log_file<<"\n";
                                delete incInf;
                            }
                            std::cout<<h_ratio<<"--"<<s_ratio<<"--"<<targ_percent<<"--"<<ev_percent<<"\n";

                        }
                    }
                    log_file.close();
                }

            }

        }

    }





    void test_sample() {
        std::string file ="resources/win95.bif";
        gum::BayesNet<double>* bn = new gum::BayesNet<double>();

        gum::BIFReader<double> reader( bn, file );
        assert(reader.proceed()==0);
        srand((int)time(0));

        auto bn_nodes=bn->dag().asNodeSet();
        std::vector<double>s {.1,.2,.3,.4,.5} ;

        for (auto i : s) {
            GUM_TRACE_VAR(i)
            GUM_TRACE_VAR(sample(bn_nodes,i)*sample(bn_nodes,i));
        }
    }


};
int main() {

    test_times test;

    test.compare_times();
    //test.test_sample();

    return 0;
}
