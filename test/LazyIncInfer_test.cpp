#define GUM_TRACE_ON
#include <iostream>
#include <fstream>
#include <assert.h>     /* assert */

#include <agrum/config.h>
#include <agrum/BN/io/BIF/BIFReader.h>
#include <agrum/core/timer.h>


//#include <ctime>
//#include <stdlib.h>
#include <agrum/multidim/multiDimArray.h>
#include <agrum/variables/labelizedVariable.h>


#include<agrum/BN/inference/lazyPropagation.h>
#include<LazyIncrementalInference.h>



class  LazyIncInfer_test {
public:

    typedef std::unique_ptr<gum::Potential<double>> __Potential;
    typedef gum::Set<const gum::Potential<double>*> __PotentialSet;

    // the function used to combine two tables
    static gum::Potential<double>*
    LPIncrmultiPotential( const gum::Potential<double>& t1,
                          const gum::Potential<double>& t2 ) {
        return new gum::Potential<double>( t1 * t2 );
    }

    // the function used to combine two tables
    static gum::Potential<double>* LPIncrprojPotential(
        const gum::Potential<double>& t1,
        const gum::Set<const gum::DiscreteVariable*>& del_vars ) {
        return new gum::Potential<double>( t1.margSumOut( del_vars ) );
    }


    gum::BayesNet<double>* bn;
    std::vector<gum::NodeId> BN_node_index;
    std::vector<const gum::DiscreteVariable*> BN_variable;
    gum::Set<const gum::DiscreteVariable*>* BN_variable_set;

    double __epsilon {1e-6};
    gum::Timer timer;

    gum::Potential<double>* joint;
    gum::MultiDimCombinationDefault<double, gum::Potential>* combination;
    gum::MultiDimProjection<double, gum::Potential>* proj;



    LazyIncInfer_test() {
        combination = new gum::MultiDimCombinationDefault<double, gum::Potential>(
            LPIncrmultiPotential );

        proj = new gum::MultiDimProjection<double, gum::Potential>(
            LPIncrprojPotential );

        BN_variable_set = new gum::Set<const gum::DiscreteVariable*>;


        // The graph used for the tests and the domain sizes:
        /*
            A             0             2
             \             \             \
              B   G         1   6         3   4
               \ / \         \ / \         \ / \
                C   H         2   7         2   3
               / \           / \           / \
              D   E         3   4         3   2
             /             /             /
            F             5             3
        */


        // create the BN
        std::vector<gum::LabelizedVariable> variables {
            gum::LabelizedVariable( "A", "", 2 ),
            gum::LabelizedVariable( "B", "", 3 ),
            gum::LabelizedVariable( "C", "", 2 ),
            gum::LabelizedVariable( "D", "", 3 ),
            gum::LabelizedVariable( "E", "", 2 ),
            gum::LabelizedVariable( "F", "", 3 ),
            gum::LabelizedVariable( "G", "", 4 ),
            gum::LabelizedVariable( "H", "", 3 )
        };

        bn = new gum::BayesNet<double>;

        for ( unsigned int i = 0; i < variables.size(); ++i ) {
            BN_node_index.push_back( bn->add( variables[i] ) );
            BN_variable.push_back( &( bn->variable( BN_node_index[i] ) ) );
            BN_variable_set->insert( BN_variable[i] );
        }

        bn->addArc( BN_node_index[0], BN_node_index[1] );
        bn->addArc( BN_node_index[1], BN_node_index[2] );
        bn->addArc( BN_node_index[2], BN_node_index[3] );
        bn->addArc( BN_node_index[2], BN_node_index[4] );
        bn->addArc( BN_node_index[3], BN_node_index[5] );
        bn->addArc( BN_node_index[6], BN_node_index[2] );
        bn->addArc( BN_node_index[6], BN_node_index[7] );

        bn->cpt( BN_node_index[0] ).fillWith( {0.2, 0.8} );
        bn->cpt( BN_node_index[1] ).fillWith( {0.3, 0.4, 0.3, 0.1, 0.3, 0.6} );
        bn->cpt( BN_node_index[2] ).fillWith( {0.1, 0.9,     // 1
                                               0.2, 0.8,     // 2
                                               0.3, 0.7,     // 3
                                               0.4, 0.6,     // 4
                                               0.5, 0.5,     // 5
                                               0.6, 0.4,     // 6
                                               0.7, 0.3,     // 7
                                               0.8, 0.2,     // 8
                                               0.9, 0.1,     // 9
                                               0.8, 0.2,     // 10
                                               0.7, 0.3,     // 11
                                               0.6, 0.4
                                              } );  // 12
        bn->cpt( BN_node_index[3] ).fillWith( {0.3, 0.2, 0.5, 0.7, 0.1, 0.2} );
        bn->cpt( BN_node_index[4] ).fillWith( {0.4, 0.6, 0.9, 0.1} );
        bn->cpt( BN_node_index[5] )
        .fillWith( {0.2, 0.4, 0.4, 0.4, 0.1, 0.5, 0.7, 0.2, 0.1} );
        bn->cpt( BN_node_index[6] ).fillWith( {0.1, 0.4, 0.2, 0.3} );
        bn->cpt( BN_node_index[7] )
        .fillWith(
        {0.4, 0.3, 0.3, 0.1, 0.8, 0.1, 0.3, 0.4, 0.3, 0.7, 0.1, 0.2} );

        // create the joint probability
        gum::Set<const gum::Potential<double>*> potset;
        for ( unsigned int i = 0; i < BN_node_index.size(); ++i ) {
            potset.insert( &( bn->cpt( i ) ) );
        }

        joint = combination->combine( potset );


    }





    bool equalPotentials( const gum::Potential<double>& p1,
                          const gum::Potential<double>& p2 ) {
        gum::Instantiation i1( p1 );
        gum::Instantiation i2( p2 );

        for ( i1.setFirst(), i2.setFirst(); !i1.end(); i1.inc(), i2.inc() ) {
            if ( ( p1[i1] == 0 ) && ( std::fabs( p2[i2] ) > __epsilon ) )
                return false;
            if ( p1[i1] > p2[i2] ) {
                if ( std::fabs( ( p1[i1] - p2[i2] ) / p1[i1] ) > __epsilon )
                    return false;
            } else {
                if ( std::fabs( ( p1[i1] - p2[i2] ) / p1[i2] ) > __epsilon )
                    return false;
            }
        }

        return true;
    }
    void testAlarm() {

        std::string file ="resources/alarm.bif";
        gum::BayesNet<double> bn;
        gum::BIFReader<double> reader(&bn, file);
        assert(reader.proceed()==0);



        gum::prm::LazyIncrementalInference incInf(&bn);
        gum::LazyPropagation<double>* lpInf=new gum::LazyPropagation<double>(&bn,
                gum::RelevantPotentialsFinderType::DSEP_BAYESBALL_POTENTIALS,
                gum::FindBarrenNodesType::FIND_BARREN_NODES,
                false);



        incInf.makeInference();
        for (auto targ: bn.nodes())incInf.addTarget(targ);

        for(const auto id:bn.nodes())  assert(equalPotentials(incInf.posterior(id),lpInf->posterior(id)));
        //GUM_TRACE_VAR(incInf.posterior(bn.idFromName("ERRLOWOUTPUT")));
        /* bn.names:
        ["CVP", , "HYPOVOLEMIA", ,"PULMEMBOLUS","INTUBATION", , "DISCONNECT", "MINVOLSET",  "VENTMACH", "VENTTUBE", "VENTLUNG", "VENTALV", "ARTCO2",
        , , "HRSAT", "INSUFFANESTH",HISTORY
        not included : "ANAPHYLAXIS", "TPR", "EXPCO2",  , "HR", "CO",,]*/

        gum::NodeSet targets;
        std::vector<std::string> ta {
            "VENTALV", "ARTCO2",
            "INTUBATION", "VENTMACH","SHUNT",
            "LVEDVOLUME","PRESS","ERRLOWOUTPUT","LVFAILURE","CATECHOL"
        };
        /*
        ,"HREKG","HYPOVOLEMIA","LVFAILURE", "HRSAT",
          "INSUFFANESTH","DISCONNECT",
          "PULMEMBOLUS", "VENTTUBE",
          "CVP","PCWP",
        */
        std::vector<std::string> t {"KINKEDTUBE", "ERRCAUTER", "FIO2", "PAP","MINVOL"};//, "PVSAT",};// "SAO2", "CATECHOL", "HRBP",  "BP",};

        for(auto i:t) {
            bn.erase(bn.idFromName(i));
        }
        incInf.setBayesNet(&bn);

        for (auto t: ta) {
            incInf.addTarget(bn.idFromName(t));
        }

        auto i1=  bn.idFromName("VENTLUNG");
        auto i2= bn.idFromName("MINVOLSET");
        auto i3= bn.idFromName("ERRLOWOUTPUT");


        gum::Potential<double> ev;
        ev << bn.variable(i1 );
        // ev.fillWith( {.25,0,.25,.5} );
        ev.fillWith( {1,0,0,0} );


        gum::List<const gum::Potential<double>*> e_list;
        e_list.insert( &ev );
        lpInf=new gum::LazyPropagation<double>(&bn,
                                               gum::RelevantPotentialsFinderType::DSEP_BAYESBALL_POTENTIALS,
                                               gum::FindBarrenNodesType::FIND_BARREN_NODES,
                                               false);
        lpInf->addEvidence( ev );
        timer.reset();
        lpInf->makeInference();
        for (auto target:incInf.targets()) auto post=lpInf->posterior(target);
        auto tLP=timer.step();
        std::cout<<"posterior computations time1 for LazyPropagation: "<<tLP <<std::endl;



        incInf.addEvidence( ev );
        timer.reset();
        incInf.makeInference();
        for (auto target:incInf.targets()) auto post=incInf.posterior(target);
        auto tInc=timer.step();
        std::cout<<"posterior computations time1 for LazyIncrementalInference: "<<tInc <<std::endl;
        for (auto target:incInf.targets()) assert(equalPotentials(incInf.posterior(target),lpInf->posterior(target)));

/// SECOND INCREMENT


        gum::Potential<double> ev1;
        ev1 << bn.variable(i2);
        // ev1.fillWith( {.5,.4,.1});
        ev1.fillWith( {0,1,0});


        lpInf=new gum::LazyPropagation<double>(&bn,
                                               gum::RelevantPotentialsFinderType::DSEP_BAYESBALL_POTENTIALS,
                                               gum::FindBarrenNodesType::FIND_BARREN_NODES,
                                               false);
        lpInf->addEvidence( ev1 );
        lpInf->addEvidence( ev );
        timer.reset();
        lpInf->makeInference();
        for (auto target:incInf.targets()) auto post=lpInf->posterior(target);
        tLP=timer.step();
        std::cout<<"posterior computations time2 for LazyPropagation: "<<tLP <<std::endl;


        incInf.addEvidence( ev1 );
        timer.reset();
        try {
            incInf.makeInference();
        } catch(gum::Exception& e) {
            GUM_SHOWERROR(e);
            throw e;
        }
        for (auto target:incInf.targets()) auto post=incInf.posterior(target);
        tInc=timer.step();
        std::cout<<"posterior computations time2 for LazyIncrementalInference: "<<tInc <<std::endl;

        for (auto target:incInf.targets()) assert(equalPotentials(incInf.posterior(target),lpInf->posterior(target)));

/// third increment

        incInf.addTarget(bn.idFromName("LVFAILURE"));
        timer.reset();
        incInf.makeInference();
        for (auto target:incInf.targets()) auto post=incInf.posterior(target);
        tInc=timer.step();
        std::cout<<"posterior computations time3 for LazyIncrementalInference: "<<tInc <<std::endl;


        lpInf=new gum::LazyPropagation<double>(&bn,
                                               gum::RelevantPotentialsFinderType::DSEP_BAYESBALL_POTENTIALS,
                                               gum::FindBarrenNodesType::FIND_BARREN_NODES,
                                               false);

        lpInf->addEvidence( ev1 );
        lpInf->addEvidence( ev );
        timer.reset();
        lpInf->makeInference();
        for (auto target:incInf.targets()) auto post=lpInf->posterior(target);
        tLP=timer.step();
        std::cout<<"posterior computations time3 for LazyPropagation: "<<tLP <<std::endl;

        for (auto target:incInf.targets())assert(equalPotentials(incInf.posterior(target),lpInf->posterior(target)));




/// FOURTH INCREMENT

        gum::Potential<double> ev3;
        ev3 << bn.variable(i3);
        ev3.fillWith( {.5,.5});

        timer.reset();
        //incInf.addEvidence(i3,0);//
        //incInf.addEvidence( ev3 );
        incInf.makeInference();
        for (auto target:incInf.targets()) auto post=incInf.posterior(target);
        tInc=timer.step();
        std::cout<<"posterior computations time4 for LazyIncrementalInference: "<<tInc <<std::endl;


        lpInf=new gum::LazyPropagation<double>(&bn,
                                               gum::RelevantPotentialsFinderType::DSEP_BAYESBALL_POTENTIALS,
                                               gum::FindBarrenNodesType::FIND_BARREN_NODES,
                                               false);


        lpInf->addEvidence( ev1 );
        lpInf->addEvidence( ev );
        lpInf->addEvidence( ev3 );
        timer.reset();
        lpInf->makeInference();
        for (auto target:incInf.targets()) auto post=lpInf->posterior(target);
        tLP=timer.step();
        std::cout<<"posterior computations time4 for LazyPropagation: "<<tLP <<std::endl;

        for (auto target:incInf.targets())assert(equalPotentials(incInf.posterior(target),lpInf->posterior(target)));





/// fifth INCREMENT
        timer.reset();
        incInf.addTarget(bn.idFromName("VENTTUBE"));
        incInf.makeInference();
        for (auto target:incInf.targets()) auto post=incInf.posterior(target);
        tInc=timer.step();
        std::cout<<"posterior computations time5 for LazyIncrementalInference: "<<tInc <<std::endl;


        lpInf=new gum::LazyPropagation<double>(&bn,
                                               gum::RelevantPotentialsFinderType::DSEP_BAYESBALL_POTENTIALS,
                                               gum::FindBarrenNodesType::FIND_BARREN_NODES,
                                               false);
        timer.reset();
        lpInf->addEvidence( ev1 );
        lpInf->addEvidence( ev );
        lpInf->addEvidence( ev3 );
        lpInf->makeInference();
        for (auto target:incInf.targets()) auto post=lpInf->posterior(target);
        tLP=timer.step();
        std::cout<<"posterior computations time5 for LazyPropagation: "<<tLP <<std::endl;

        for (auto target:incInf.targets())assert(equalPotentials(incInf.posterior(target),lpInf->posterior(target)));


    }



    void testMarginal() {
        gum::BayesNet<double>* bn=new gum::BayesNet<double>();
        gum::NodeId i1, i2, i3, i4, i5;
        gum::Potential<double>* e_i1, *e_i4;


        gum::LabelizedVariable n1( "1", "", 2 ), n2( "2", "", 2 ),
            n3( "3", "", 2 );
        gum::LabelizedVariable n4( "4", "", 2 ), n5( "5", "", 3 );

        i1 = bn->add( n1 );
        i2 = bn->add( n2 );
        i3 = bn->add( n3 );
        i4 = bn->add( n4 );
        i5 = bn->add( n5 );

        bn->addArc( i1, i3 );
        bn->addArc( i1, i4 );
        bn->addArc( i3, i5 );
        bn->addArc( i4, i5 );
        bn->addArc( i2, i4 );
        bn->addArc( i2, i5 );

        bn->cpt( i1 ).fillWith( {0.2, 0.8} );
        bn->cpt( i2 ).fillWith( {0.3, 0.7} );
        bn->cpt( i3 ).fillWith( {0.1, 0.9, 0.9, 0.1} );
        bn->cpt( i4 ).fillWith( { // clang-format off
            0.4, 0.6,
            0.5, 0.5,
            0.5, 0.5,
            1.0, 0.0
        } );  // clang-format on
        bn->cpt( i5 ).fillWith( { // clang-format off
            0.3, 0.6, 0.1,
            0.5, 0.5, 0.0,
            0.5, 0.5, 0.0,
            1.0, 0.0, 0.0,
            0.4, 0.6, 0.0,
            0.5, 0.5, 0.0,
            0.5, 0.5, 0.0,
            0.0, 0.0, 1.0
        } );                                     // clang-format on

        e_i1 = new gum::Potential<double>();
        ( *e_i1 ) << bn->variable( i1 );
        e_i1->fill( 0.0 );
        gum::Instantiation inst_1( *e_i1 );
        inst_1.chgVal( bn->variable( i1 ), 0 );
        e_i1->set( inst_1, 1.0 );

        e_i4 = new gum::Potential<double>();
        ( *e_i4 ) << bn->variable( i4 );
        e_i4->fill( 0.0 );
        gum::Instantiation inst_4( *e_i4 );
        inst_4.chgVal( bn->variable( i4 ), 1 );
        e_i4->set( inst_4, 1.0 );


        gum::prm::LazyIncrementalInference inf( bn );
        for (auto node :bn->nodes()) inf.addTarget(node );
        gum::LazyPropagation<double> inf2( bn );

        ( inf.makeInference() );

        inf.makeInference();
        inf2.makeInference();

        assert( equalPotentials( inf.posterior( i1 ), inf2.posterior( i1 ) ) );
        assert( equalPotentials( inf.posterior( i2 ), inf2.posterior( i2 ) ) );
        assert( equalPotentials( inf.posterior( i3 ), inf2.posterior( i3 ) ) );
        assert( equalPotentials( inf.posterior( i4 ), inf2.posterior( i4 ) ) );
        assert( equalPotentials( inf.posterior( i5 ), inf2.posterior( i5 ) ) );




//// test with evidences
        gum::List<const gum::Potential<double>*> e_list;
        e_list.insert( e_i1 );
        e_list.insert( e_i4 );

        gum::prm::LazyIncrementalInference inf_( bn );
        for (auto node :bn->nodes()) inf_.addTarget(node );
        gum::LazyPropagation<double> infX( bn );

        for ( auto pot : e_list ) {
            inf_.addEvidence( *pot ) ;
            infX.addEvidence(  *pot  );

        }

        ( inf_.makeInference() );
        ( infX.makeInference() );
        ( inf_.posterior( i1 ) );
        ( inf_.posterior( i2 ) );
        ( inf_.posterior( i3 ) );

        ( inf_.posterior( i4 ) );
        ( inf_.posterior( i5 ) );

        assert( equalPotentials( inf_.posterior( i1 ), infX.posterior( i1 ) ) );
        assert( equalPotentials( inf_.posterior( i2 ), infX.posterior( i2 ) ) );
        assert( equalPotentials( inf_.posterior( i3 ), infX.posterior( i3 ) ) );
        assert( equalPotentials( inf_.posterior( i4 ), infX.posterior( i4 ) ) );
        assert( equalPotentials( inf_.posterior( i5 ), infX.posterior( i5 ) ) );

        gum::prm::LazyIncrementalInference inf3( bn );
        for (auto node :bn->nodes()) inf3.addTarget(node );
        gum::LazyPropagation<double> inf2X( bn );

        ( inf3.addEvidence( i1, 0 ) );
        ( inf3.addEvidence( i4, 1 ) );
        ( inf2X.addEvidence( i1, 0 ) );
        ( inf2X.addEvidence( i4, 1 ) );

        ( inf3.makeInference() );
        ( inf2X.makeInference() );


        assert(
            equalPotentials( inf3.posterior( i1 ), inf2X.posterior( i1 ) ) );
        assert(
            equalPotentials( inf3.posterior( i2 ), inf2X.posterior( i2 ) ) );
        assert(
            equalPotentials( inf3.posterior( i3 ), inf2X.posterior( i3 ) ) );
        assert(
            equalPotentials( inf3.posterior( i4 ), inf2X.posterior( i4 ) ) );
        assert(
            equalPotentials( inf3.posterior( i5 ), inf2X.posterior( i5 ) ) );

    }


    void testAsia() {
        std::string file = "resources/asia.bif" ;
        gum::BayesNet<double> net;
        gum::BIFReader<double> reader( &net, file );

        assert( 0== reader.proceed() );
        assert( reader.warnings()==(gum::Size)0 );



        for ( auto node : net.dag() ) {
            const auto& variable = net.variable( node );
            gum::Potential<double> ev_pot;
            ev_pot << variable;
            ev_pot.fill( 0.0 );
            gum::List<const gum::Potential<double>*> evidences;
            evidences.insert( &ev_pot );

            gum::Instantiation inst( ev_pot );
            for ( inst.setFirst(); !inst.end(); ++inst ) {
                ev_pot.set( inst,1.0 );
                gum::prm::LazyIncrementalInference inf1( &net );
                for (auto node :net.nodes()) inf1.addTarget(node );
                gum::LazyPropagation<double> inf2( &net );
                for ( auto pot : evidences ) {
                    inf1.addEvidence( *pot );
                    inf2.addEvidence(  *pot  );
                }


                inf2.makeInference();
                inf1.makeInference();
                for ( auto node : net.dag() ) {
                    assert( equalPotentials( inf1.posterior( node ),
                                             inf2.posterior( node ) ) );
                }
                ev_pot.set( inst, 0.0 );
            }
        }
    }


    void testAsia3() {
        std::string file = "resources/asia3.bif" ;
        gum::BayesNet<double> bn;
        gum::BIFReader<double> reader(&bn, file);
        assert(reader.proceed()==0);


        for ( auto node : bn.dag() ) {
            const auto& variable = bn.variable( node );
            gum::Potential<double> ev_pot;
            ev_pot << variable;
            ev_pot.fill( 0.0 );

            gum::Instantiation inst( ev_pot );
            for ( inst.setFirst(); !inst.end(); ++inst ) {
                ev_pot.set( inst, 1.0 );

                for ( auto node2 : bn.dag() ) {
                    if ( node2 > node ) {
                        const auto& variable2 = bn.variable( node2 );
                        gum::Potential<double> ev_pot2;
                        ev_pot2 << variable2;
                        ev_pot2.fill( 0.0 );

                        gum::List<const gum::Potential<double>*> evidences;
                        evidences.insert( &ev_pot );
                        evidences.insert( &ev_pot2 );
                        // std::cout << "hard ev: " << node << "  " << node2 << std::endl;

                        gum::Instantiation inst2( ev_pot2 );
                        for ( inst2.setFirst(); !inst2.end(); ++inst2 ) {
                            ev_pot2.set( inst2, 1.0 );

                            gum::prm::LazyIncrementalInference inf1(&bn);
                            for (auto node :bn.nodes()) inf1.addTarget(node );
                            gum::LazyPropagation<double> inf2( &bn );
                            for ( auto pot : evidences ) {
                                ( inf1.addEvidence( *pot ) );
                                ( inf2.addEvidence( *pot ) );
                            }

                            ( inf1.makeInference() );
                            ( inf2.makeInference() );
                            for ( auto xnode : bn.dag() ) {
                                assert( equalPotentials( inf1.posterior( xnode ),
                                                         inf2.posterior( xnode ) ) );

                            }
                            ev_pot2.set( inst2, 0.0 );
                        }
                    }
                }

                ev_pot.set( inst, 0.0 );
            }
        }
    }


    gum::Potential<double>* create_evidence( const gum::NodeId node_id,
            const std::vector<double>& ev ) {
        gum::Potential<double>* proba = new gum::Potential<double>;
        proba->add( *( BN_variable[node_id] ) );
        proba->fillWith( ev );

        return proba;
    }
    gum::Potential<double>*
    posterior_joint( const gum::Potential<double>* joint,
                     gum::Set<const gum::Potential<double>*> evidence ) {
        evidence.insert( joint );
        gum::Potential<double>* joint_pot = combination->combine( evidence );

        return joint_pot;
    }


    gum::Potential<double>* get_marginal( const gum::Potential<double>* joint,
                                          const gum::NodeId target_id ) {
        // get the set of variables to erase
        gum::Set<const gum::DiscreteVariable*> myset = *BN_variable_set;
        myset.erase( BN_variable[target_id] );
        gum::Potential<double>* res = LPIncrprojPotential( *joint, myset );
        res->normalize();
        return res;
    }



    gum::Potential<double>* get_joint( const gum::Potential<double>* joint,
                                       const gum::NodeSet& target_ids ) {
        // get the set of variables to erase
        gum::Set<const gum::DiscreteVariable*> myset = *BN_variable_set;
        for ( auto target_id : target_ids )
            myset.erase( BN_variable[target_id] );
        return proj->project( *joint, myset );
    }






    void testPot() {
        gum::Potential<double> p1( new gum::MultiDimArray<double>() );
        gum::Potential<double> p2( new gum::MultiDimArray<double>() );

        gum::LabelizedVariable a( "a", "first var", 2 ),
            b( "b", "second var", 2 ), c( "c", "third var", 2 );
        p1 << a << b ;
        p2 <<c;

        p1.fillWith( std::vector<double> {0.1, 0.9, 0.8, 0.2} );
        p2.fillWith( std::vector<double> {0.3, 0.7} );
        std::cout<< p1<<"\n";
    }



    void test_prior() {
        gum::prm::LazyIncrementalInference inf( bn );
        for (auto node :bn->nodes()) inf.addTarget(node );
        ( inf.makeInference() );

        // get the marginals of A, C, D, H
        std::unique_ptr<gum::Potential<double>> pa(
                get_marginal( joint, BN_node_index[0] ) ),
                              pc( get_marginal( joint, BN_node_index[2] ) ),
                              pd( get_marginal( joint, BN_node_index[3] ) ),
                              ph( get_marginal( joint, BN_node_index[7] ) );

        assert    ( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
        assert    ( equalPotentials( inf.posterior( BN_node_index[2] ), *pc ) );
        assert    ( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        assert    ( equalPotentials( inf.posterior( BN_node_index[7] ), *ph ) );
    }


    void test_prior_with_targets() {
        gum::prm::LazyIncrementalInference inf( bn );
        inf.addTarget( 0 );  // A
        inf.addTarget( 2 );  // C

        ( inf.makeInference() );

        // get the marginals of A, C, D
        __Potential pa( get_marginal( joint, BN_node_index[0] ) );
        __Potential pc( get_marginal( joint, BN_node_index[2] ) );



        assert ( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
        assert( equalPotentials( inf.posterior( BN_node_index[2] ), *pc ) );
    }

    void test_prior_with_targets_evidence() {
        gum::prm::LazyIncrementalInference inf( bn );
        inf.addTarget( 0 );  // A
        inf.addTarget( 5 );  // F

        __Potential ev1( create_evidence( 1, {0, 0, 1} ) );
        inf.addEvidence( 1, 2 );
        __Potential ev3( create_evidence( 3, {1, 0,0} ) );
        inf.addEvidence( *ev3 );

        __PotentialSet evset;
        evset.insert( ev1.get() );
        evset.insert( ev3.get() );
        __Potential posterior( posterior_joint( joint, evset ) );

        ( inf.makeInference() );

        // get the marginals of A, F
        std::unique_ptr<gum::Potential<double>> pa(
                get_marginal( posterior.get(), BN_node_index[0] ) ),
                              pf( get_marginal( posterior.get(), BN_node_index[5] ) );

        assert ( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
        assert( equalPotentials( inf.posterior( BN_node_index[5] ), *pf ) );
    }


    void test_prior_with_targets_outside_evidence() {
        gum::prm::LazyIncrementalInference inf( bn );

        inf.addTarget( 0 );  // A
        inf.addTarget( 3 );  // D

        __Potential ev0( create_evidence( 0, {0.3, 0.7} ) );
        inf.addEvidence( *ev0 );
        __Potential ev1( create_evidence( 1, {0, 0, 1} ) );
        inf.addEvidence( *ev1 );
        __Potential ev7( create_evidence( 7, {0.4, 0.2, 0.3} ) );
        inf.addEvidence( *ev7 );

        __PotentialSet evset;
        evset.insert( ev0.get() );
        evset.insert( ev1.get() );
        evset.insert( ev7.get() );
        __Potential posterior( posterior_joint( joint, evset ) );

        ( inf.makeInference() );

        // get the marginals of A, D
        std::unique_ptr<gum::Potential<double>> pa(
                get_marginal( posterior.get(), BN_node_index[0] ) ),
                              pd( get_marginal( posterior.get(), BN_node_index[3] ) );

        assert( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
        assert( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
    }



    void test_prior_with_targets_evidence_values_changed() {
        gum::prm::LazyIncrementalInference inf( bn );

        inf.addTarget( 0 );  // A
        inf.addTarget( 3 );  // D

        __Potential ev0( create_evidence( 0, {0, 1} ) );
        inf.addEvidence( *ev0 );
        __Potential ev1( create_evidence( 1, {1,0,0} ) );
        inf.addEvidence( *ev1 );
        __Potential ev7( create_evidence( 7, {0,0,1} ) );
        inf.addEvidence( *ev7 );


        __PotentialSet evset;
        evset.insert( ev0.get() );
        evset.insert( ev1.get() );
        evset.insert( ev7.get() );

        {
            __Potential posterior( posterior_joint( joint, evset ) );

            ( inf.makeInference() );

            // get the marginals of A, D
            std::unique_ptr<gum::Potential<double>> pa(
                    get_marginal( posterior.get(), BN_node_index[0] ) ),
                                  pd( get_marginal( posterior.get(), BN_node_index[3] ) );

            assert( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
            assert( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        }


        __Potential evp0( create_evidence( 0, {1, 0} ) );
        inf.chgEvidence( 0, 0 );
        __Potential evp1( create_evidence( 1, {0.8, 0.4, 0.1} ) );
        inf.chgEvidence( 1, {0.8, 0.4, 0.1} );
        __Potential evp7( create_evidence( 7, {0.2, 0.3, 0.6} ) );
        inf.chgEvidence( *evp7 );

        evset.clear();
        evset.insert( evp0.get() );
        evset.insert( evp1.get() );
        evset.insert( evp7.get() );

        {
            __Potential posterior( posterior_joint( joint, evset ) );

            ( inf.makeInference() );

            // get the marginals of A, D
            std::unique_ptr<gum::Potential<double>> pa(
                    get_marginal( posterior.get(), BN_node_index[0] ) ),
                                  pd( get_marginal( posterior.get(), BN_node_index[3] ) );

            ( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
            ( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        }


        __Potential evpp7( create_evidence( 7, {0.9, 0.05, 0.05} ) );
        inf.chgEvidence( *evpp7 );

        evset.clear();
        evset.insert( evp0.get() );
        evset.insert( evp1.get() );
        evset.insert( evpp7.get() );

        {
            __Potential posterior( posterior_joint( joint, evset ) );

            ( inf.makeInference() );

            return;

            // get the marginals of A, D
            std::unique_ptr<gum::Potential<double>> pa(
                    get_marginal( posterior.get(), BN_node_index[0] ) ),
                                  pd( get_marginal( posterior.get(), BN_node_index[3] ) );

            assert( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
            assert ( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        }


        inf.chgEvidence( *ev0 );
        inf.chgEvidence( *evp7 );
        evset.clear();
        evset.insert( ev0.get() );
        evset.insert( evp1.get() );
        evset.insert( evp7.get() );

        {
            __Potential posterior( posterior_joint( joint, evset ) );

            ( inf.makeInference() );

            return;

            // get the marginals of A, D
            std::unique_ptr<gum::Potential<double>> pa(
                    get_marginal( posterior.get(), BN_node_index[0] ) ),
                                  pd( get_marginal( posterior.get(), BN_node_index[3] ) );

            assert( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
            assert( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        }

        inf.eraseEvidence( 0 );
        evset.clear();
        evset.insert( evp1.get() );
        evset.insert( evp7.get() );

        {
            __Potential posterior( posterior_joint( joint, evset ) );

            ( inf.makeInference() );
            // get the marginals of A, D
            std::unique_ptr<gum::Potential<double>> pa(
                    get_marginal( posterior.get(), BN_node_index[0] ) ),
                                  pd( get_marginal( posterior.get(), BN_node_index[3] ) );

            assert( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
            assert( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        }


        inf.addEvidence( *evp0 );
        ( inf.makeInference() );
        inf.eraseEvidence( 0 );

        {
            __Potential posterior( posterior_joint( joint, evset ) );

            ( inf.makeInference() );

            // get the marginals of A, D
            std::unique_ptr<gum::Potential<double>> pa(
                    get_marginal( posterior.get(), BN_node_index[0] ) ),
                                  pd( get_marginal( posterior.get(), BN_node_index[3] ) );

            assert ( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
            assert( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        }

    }


    void test_prior_with_targets_evidence_changed() {
        gum::prm::LazyIncrementalInference inf( bn );

        inf.addTarget( 0 );  // A
        inf.addTarget( 3 );  // D

        __Potential ev0( create_evidence( 0, {1,0} ) );
        inf.addEvidence( *ev0 );
        __Potential ev1( create_evidence( 1, {0.3, 0.1, 0.6} ) );
        inf.addEvidence( *ev1 );
        __Potential ev7( create_evidence( 7, {0.5, 0.2, 0.3} ) );
        inf.addEvidence( *ev7 );

        __PotentialSet evset;
        evset.insert( ev0.get() );
        evset.insert( ev1.get() );
        evset.insert( ev7.get() );

        {
            __Potential posterior( posterior_joint( joint, evset ) );
            inf.makeInference();

            // get the marginals of A, D
            std::unique_ptr<gum::Potential<double>>
                                                 pa( get_marginal( posterior.get(), BN_node_index[0] ) ),
                                                 pd( get_marginal( posterior.get(), BN_node_index[3] ) );
            assert ( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
            assert ( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        }


        __Potential ev4( create_evidence( 4, {1,0} ) );
        __Potential evp7( create_evidence( 7, {0,0,1} ) );
        inf.eraseEvidence( 0 );
        inf.addEvidence( *ev4 );
        inf.chgEvidence( *evp7 );
        evset.clear();
        evset.insert( ev1.get() );
        evset.insert( ev4.get() );
        evset.insert( evp7.get() );

        {

            __Potential posterior( posterior_joint( joint, evset ) );
            try {
                ( inf.makeInference() );
            } catch(gum::Exception& e) {
                GUM_SHOWERROR(e);
                throw e;
            }
            // get the marginals of A, D
            std::unique_ptr<gum::Potential<double>> pa(
                    get_marginal( posterior.get(), BN_node_index[0] ) ),
                                  pd( get_marginal( posterior.get(), BN_node_index[3] ) );
            assert ( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
            assert ( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        }

        __Potential evp0( create_evidence( 0, {1, 0} ) );
        __Potential evpp4( create_evidence( 4, {1,0} ) );
        __Potential evpp7( create_evidence( 7, {0, .1, .9} ) );
        inf.addEvidence( 0, 0 );
        inf.chgEvidence( *evpp4 );
        inf.chgEvidence( *evpp7 );
        evset.clear();
        evset.insert( evp0.get() );
        evset.insert( ev1.get() );
        evset.insert( evpp4.get() );
        evset.insert( evpp7.get() );

        {
            __Potential posterior( posterior_joint( joint, evset ) );
            ( inf.makeInference() );
            // get the marginals of A, D
            std::unique_ptr<gum::Potential<double>>
                                                 pa(get_marginal( posterior.get(), BN_node_index[0] ) ),
                                                 pd( get_marginal( posterior.get(), BN_node_index[3] ) );

            assert ( equalPotentials( inf.posterior( BN_node_index[0] ), *pa ) );
            assert ( equalPotentials( inf.posterior( BN_node_index[3] ), *pd ) );
        }
    }


    void test_asia_incremental() {
        std::string file ="resources/asia.bif";
        gum::BayesNet<double>* bn = new gum::BayesNet<double>();

        gum::BIFReader<double> reader( bn, file );
        assert(reader.proceed()==0);


        gum::prm::LazyIncrementalInference* incInf= new gum::prm::LazyIncrementalInference(bn);
        for (auto node: incInf->BayesNet().nodes()) incInf->addTarget(node);
//         gum::LazyPropagation<double>* incInf = new  gum::LazyPropagation<double>(bn);

        for (gum::NodeId i=0; i<6; ++i) {
            i+=2;
            GUM_TRACE_VAR(i)
            incInf->addEvidence(i,1);

            try {
                incInf->makeInference();
            } catch(gum::Exception& e) {
                GUM_SHOWERROR(e);
                throw e;
            }


            for (auto node: incInf->BayesNet().nodes()) {
                try {
                    incInf->posterior(node);
                } catch(gum::Exception& e) {
                    GUM_SHOWERROR(e);
                    throw e;
                }
            }

        }
    }
};

//
//int main(void) {
//    gum::Potential<double> p;
//    LazyIncInfer_test tst1;
//
//    tst1.testAlarm();
//    GUM_TRACE("testAlarm passed")
//
//    tst1.testAsia3();
//    GUM_TRACE("testAsia3 passed")
//    tst1.testAsia() ;
//    GUM_TRACE("testAsia passed")
//
//    tst1.test_asia_incremental();
//    GUM_TRACE("test_asia_incremental passed")
//
//    tst1.testMarginal() ;
//    GUM_TRACE("testMarginal passed")
//
//    (tst1.test_prior());
//    GUM_TRACE("test_prior passed")
//
//    tst1.test_prior_with_targets();
//    GUM_TRACE("test_prior_with_targets passed")
//    tst1.test_prior_with_targets_evidence();
//    GUM_TRACE("test_prior_with_targets_evidence passed")
//
//
//    tst1.test_prior_with_targets_evidence_values_changed();
//    GUM_TRACE("test_prior_with_targets_evidence_values_changed passed")
//
//
//    tst1.test_prior_with_targets_outside_evidence();
//    GUM_TRACE("test_prior_with_targets_outside_evidence passed")
//
//
//    tst1.test_prior_with_targets_evidence_changed();
//    GUM_TRACE("test_prior_with_targets_evidence_changed passed")
//
//    return 0;
//
//}
//
//
//
//
