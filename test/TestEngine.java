
public class TestEngine {

    public static void printerTest() {
        org.agrum.prmEngine.IncPRMengine eng = new org.agrum.prmEngine.IncPRMengine();
        eng.readPRM("../resources/printers_system.o3prm","sys", "../resources/");

        // add instance
        eng.addInstance("PowerSupply", "newPow");
        eng.addInstance("Room", "newR");
        eng.addInstance("Printer", "newPrinter0");
        eng.addInstance("Printer", "newPrinter1", "printers");
        eng.addInstance("Computer", "newComp");

        //set relations
        eng.addRelation("newR", "power", "newPow");
        eng.addRelation("newPrinter0","room","newR");
        eng.addRelation("newPrinter1","room","newR");
        eng.addRelation("newComp","room","newR");
        eng.addRelation("newComp","printers","printers");

        // instantiate instance
        eng.instantiateSystem();

        //evidence
        System.out.println("before evidence : P(printers[1].hasPaper=1)=" + eng.getPosterior("printers[1]", "hasPaper", 1));
        eng.addEvidence("printers[1]", "hasPaper", 1);
        eng.makeInference();
        System.out.println("after evidence: P(printers[1].hasPaper=1)=" + eng.getPosterior("printers[1]", "hasPaper", 1));

        eng.removeEvidence("printers[1]", "hasPaper");
        System.out.println("after removing evidence: P(printers[1].hasPaper=1)=" + eng.getPosterior("printers[1]", "hasPaper", 1));
        eng.makeInference();
        //eng.removeAllEvidences();

        //target
        eng.addTarget("p1", "equipState");
        //eng.addTarget("p2", "equipState");
        //eng.addTarget("c1", "equipState");
        //eng.addTarget("c1", "canPrint");

        //query

        System.out.println("  P(p1.equipState=OK)=" + eng.getPosterior("printers[1]", "equipState", 0));
        //  System.out.println("  P(newPow.powState=true)=" + eng.getPosterior("newPow","powState",0));

        // remove instance
        eng.removeInstance("newPrinter0");

        eng.printAllSystems();

        for(int i = 0; i < 1; i++){
            System.out.println(eng.testRandomPosterior());
        }

    }

    public static void prmExplorerTest() {
        org.agrum.prmEngine.IncPRMengine eng = new org.agrum.prmEngine.IncPRMengine();

        eng.readPRM("../resources/ComplexPrinters/complexprinters_system.o3prm","aSys", "../resources/ComplexPrinters/");
        //eng.readPRM("../resources/complexPrinters_system.o3prm","Work", "../resources/");
        //eng.readPRM("../ressources/ComplexPrinters/fr/lip6/printers/system.o3prm","Work", "../resources/ComplexPrinters");

        eng.printAllSystems();

        System.out.println("test2\n");

        eng.printClasses();

        System.out.println("test3\n");

        eng.printTypes();

        System.out.println("test4\n");

        eng.printInterfaces();
    }

    public static void testEmptyAgg() {
        org.agrum.prmEngine.IncPRMengine eng = new org.agrum.prmEngine.IncPRMengine();

        eng.readPRM("../resources/test.o3prm","sys", "../resources/");
        eng.instantiateSystem();

        //eng.printNodes();

        eng.addInstance("ClasseA","a1");
        eng.addInstance("ClasseA","a2");

        eng.addInstance("ClasseB","b1");
        eng.addInstance("ClasseB","b2");

        eng.addRelation("b1","classeA","a1");
        eng.addRelation("b2","classeA","a2");

        eng.addInstance("ClasseC","c1");

        eng.instantiateSystem();

        eng.printBN();

        System.out.println("Value of test : [" + eng.getPosterior("b1", "testB",0) +"," + eng.getPosterior("b1", "testB",1)+ "," + eng.getPosterior("b1", "testB",2)+"]");
        System.out.println("Value of agg : [" + eng.getPosterior("c1", "state",0) +"," + eng.getPosterior("c1", "state",1)+ "," + eng.getPosterior("c1", "state",2)+"]");

        eng.addInstance("ClasseA","a3");

        eng.addRelation("c1","classesB","b1");
        eng.addRelation("c1","classesB","b2");

        eng.addEvidence("b1","testB",2);
        eng.addEvidence("b2","testB",1);

        //eng.instantiateSystem();

        eng.printBN();

        System.out.println("Value of test : [" + eng.getPosterior("b1", "testB",0) +"," + eng.getPosterior("b1", "testB",1)+ "," + eng.getPosterior("b1", "testB",2)+"]");
        System.out.println("Value of agg : [" + eng.getPosterior("c1", "state",0) +"," + eng.getPosterior("c1", "state",1)+ "," + eng.getPosterior("c1", "state",2)+"]");
    }

    public static void testEmptyAgg2() {
        org.agrum.prmEngine.IncPRMengine eng = new org.agrum.prmEngine.IncPRMengine();

        System.out.println("-------------------------------------------------------");
        System.out.println("Test with the aggregator declared outside of the system\n");
        System.out.println("-------------------------------------------------------");

        System.out.println("\nSystem instantiation : \n");

        eng.readPRM("../resources/test2.o3prm","sys", "../resources/");
        eng.instantiateSystem();

        System.out.println("\nSystem instantiated");
        System.out.println("Adding nodes :\n");


        eng.addInstance("ClasseA","a1");
        eng.addInstance("ClasseA","a2");

        eng.addInstance("ClasseB","b1");

        eng.instantiateSystem();

        System.out.print("Nodes added.");

        System.out.println("\nBefore modifications : \n");

        System.out.println("Value of a1 : [" + eng.getPosterior("a1", "testA",0) +"," + eng.getPosterior("a1", "testA",1)+ "," + eng.getPosterior("a1", "testA",2)+"]");
        System.out.println("Value of a2 : [" + eng.getPosterior("a2", "testA",0) +"," + eng.getPosterior("a2", "testA",1)+ "," + eng.getPosterior("a2", "testA",2)+"]");
        System.out.println("Value of agg : [" + eng.getPosterior("b1", "state",0) +"," + eng.getPosterior("b1", "state",1)+ "," + eng.getPosterior("b1", "state",2)+"]\n");
        
        eng.addRelation("b1","classesA","a1");
        eng.addRelation("b1","classesA","a2");

        eng.addEvidence("a1","testA",1);
        eng.addEvidence("a2","testA",2);

        System.out.println("\nAfter modifications : \n");

        eng.instantiateSystem();

        //eng.printBN();

        System.out.println("Value of a1 : [" + eng.getPosterior("a1", "testA",0) +"," + eng.getPosterior("a1", "testA",1)+ "," + eng.getPosterior("a1", "testA",2)+"]");
        System.out.println("Value of a2 : [" + eng.getPosterior("a2", "testA",0) +"," + eng.getPosterior("a2", "testA",1)+ "," + eng.getPosterior("a2", "testA",2)+"]");
        System.out.println("Value of agg : [" + eng.getPosterior("b1", "state",0) +"," + eng.getPosterior("b1", "state",1)+ "," + eng.getPosterior("b1", "state",2)+"]\n");
    }

    public static void testEmptyAgg3() {
        org.agrum.prmEngine.IncPRMengine eng = new org.agrum.prmEngine.IncPRMengine();

        System.out.println("-----------------------------------------------");
        System.out.println("Test with the aggregator declared in the system\n");
        System.out.println("-----------------------------------------------");

        System.out.println("\nSystem instantiation : \n");

        eng.readPRM("../resources/test3.o3prm","sys", "../resources/");
        eng.instantiateSystem();

        System.out.println("\nSystem instantiated");
        System.out.println("Adding nodes :\n");

        //eng.addInstance("ClasseA","a1");
        eng.addInstance("ClasseA","a2");

        eng.instantiateSystem();

        System.out.print("Nodes added.");
        System.out.println("\nBefore modifications : \n");

        System.out.println("Value of a1 : [" + eng.getPosterior("a1", "testA",0) +"," + eng.getPosterior("a1", "testA",1)+ "," + eng.getPosterior("a1", "testA",2)+"]");
        System.out.println("Value of a2 : [" + eng.getPosterior("a2", "testA",0) +"," + eng.getPosterior("a2", "testA",1)+ "," + eng.getPosterior("a2", "testA",2)+"]");
        System.out.println("Value of agg : [" + eng.getPosterior("b1", "state",0) +"," + eng.getPosterior("b1", "state",1)+ "," + eng.getPosterior("b1", "state",2)+"]\n");
        
        eng.addRelation("b1","classesA","a1");
        eng.addRelation("b1","classesA","a2");

        eng.addEvidence("a1","testA",1);
        eng.addEvidence("a2","testA",2);

        System.out.println("\nAfter modifications : \n");

        eng.instantiateSystem();

        //eng.printBN();

        System.out.println("Value of a1 : [" + eng.getPosterior("a1", "testA",0) +"," + eng.getPosterior("a1", "testA",1)+ "," + eng.getPosterior("a1", "testA",2)+"]");
        System.out.println("Value of a2 : [" + eng.getPosterior("a2", "testA",0) +"," + eng.getPosterior("a2", "testA",1)+ "," + eng.getPosterior("a2", "testA",2)+"]");
        System.out.println("Value of agg : [" + eng.getPosterior("b1", "state",0) +"," + eng.getPosterior("b1", "state",1)+ "," + eng.getPosterior("b1", "state",2)+"]");
    }

    public static void testEmptyAgg4() {
        org.agrum.prmEngine.IncPRMengine eng = new org.agrum.prmEngine.IncPRMengine();

        System.out.println("-------------------------------------------------------");
        System.out.println("Test with the aggregator declared outside of the system\n");
        System.out.println("-------------------------------------------------------");

        System.out.println("\nSystem instantiation : \n");

        eng.readPRM("../resources/test2.o3prm","sys", "../resources/");
        eng.instantiateSystem();

        System.out.println("\nSystem instantiated");
        System.out.println("Adding nodes :\n");


        eng.addInstance("ClasseA","a1");
        eng.addInstance("ClasseA","a2");

        eng.addInstance("ClasseB","b1");

        eng.instantiateSystem();

        System.out.print("Nodes added.");

        System.out.println("\nBefore modifications : \n");

        System.out.println("Value of a1 : [" + eng.getPosterior("a1", "testA",0) +"," + eng.getPosterior("a1", "testA",1)+ "," + eng.getPosterior("a1", "testA",2)+"]");
        System.out.println("Value of a2 : [" + eng.getPosterior("a2", "testA",0) +"," + eng.getPosterior("a2", "testA",1)+ "," + eng.getPosterior("a2", "testA",2)+"]");
        System.out.println("Value of agg : [" + eng.getPosterior("b1", "state",0) +"," + eng.getPosterior("b1", "state",1)+ "," + eng.getPosterior("b1", "state",2)+"]\n");
        
        eng.addRelation("b1","classesA","a1");

        eng.addEvidence("a1","testA",2);

        System.out.println("\nAfter modifications : \n");

        eng.instantiateSystem();

        //eng.printBN();

        System.out.println("Value of a1 : [" + eng.getPosterior("a1", "testA",0) +"," + eng.getPosterior("a1", "testA",1)+ "," + eng.getPosterior("a1", "testA",2)+"]");
        System.out.println("Value of a2 : [" + eng.getPosterior("a2", "testA",0) +"," + eng.getPosterior("a2", "testA",1)+ "," + eng.getPosterior("a2", "testA",2)+"]");
        System.out.println("Value of agg : [" + eng.getPosterior("b1", "state",0) +"," + eng.getPosterior("b1", "state",1)+ "," + eng.getPosterior("b1", "state",2)+"]\n");
    
        eng.addRelation("b1","classesA","a2");

        eng.addEvidence("a2","testA",1);

        System.out.println("\nAfter modifications : \n");

        eng.instantiateSystem();

        //eng.printBN();

        System.out.println("Value of a1 : [" + eng.getPosterior("a1", "testA",0) +"," + eng.getPosterior("a1", "testA",1)+ "," + eng.getPosterior("a1", "testA",2)+"]");
        System.out.println("Value of a2 : [" + eng.getPosterior("a2", "testA",0) +"," + eng.getPosterior("a2", "testA",1)+ "," + eng.getPosterior("a2", "testA",2)+"]");
        System.out.println("Value of agg : [" + eng.getPosterior("b1", "state",0) +"," + eng.getPosterior("b1", "state",1)+ "," + eng.getPosterior("b1", "state",2)+"]\n");
    }

    public static void testRelationModif(){
       org.agrum.prmEngine.IncPRMengine eng = new org.agrum.prmEngine.IncPRMengine();

        System.out.println("--------------------");
        System.out.println("Test remove Relation\n");
        System.out.println("--------------------");

        System.out.println("\nSystem instantiation : \n");

        eng.readPRM("../resources/test2.o3prm","sys", "../resources/");
        eng.instantiateSystem();

        System.out.println("\nSystem instantiated");

        System.out.println("Adding nodes :\n");

        eng.addInstance("ClasseA","a1");
        eng.addInstance("ClasseB","b1");

        eng.instantiateSystem();

        System.out.print("Nodes added.");

        System.out.println("\nAdding relation : \n");
        eng.addRelation("b1","classesA","a1");
        eng.instantiateSystem();
        System.out.println("==> Ok");

        System.out.println("\nRemoving relation : \n");
        eng.removeRelation("b1","classesA","a1");
        eng.instantiateSystem();
        System.out.println("==> Ok");

        System.out.println("\nAdding relation and removing it in the same time : \n");
        eng.addRelation("b1","classesA","a1");
        eng.removeRelation("b1","classesA","a1");
        eng.instantiateSystem();
        System.out.println("==> Ok");

        System.out.println("\nRemoving relation when the relation doesn't exist : \n");
        try{
            eng.removeRelation("b1","classesA","a1");
            eng.instantiateSystem();
        }catch(Exception e){
            System.out.println("Exception catched");
        }
        System.out.println("==> Ok");
    }

    public static void testInstanceModif(){
       org.agrum.prmEngine.IncPRMengine eng = new org.agrum.prmEngine.IncPRMengine();

        System.out.println("--------------------");
        System.out.println("Test remove Relation\n");
        System.out.println("--------------------");

        System.out.println("\nSystem instantiation : \n");

        eng.readPRM("../resources/test2.o3prm","sys", "../resources/");
        eng.instantiateSystem();

        System.out.println("\nSystem instantiated");

        System.out.println("Adding nodes :\n");

        eng.addInstance("ClasseA","a1");
        eng.addInstance("ClasseB","b1");

        eng.instantiateSystem();

        System.out.print("Nodes added.");

        System.out.println("\nRemoving instance : \n");
        eng.removeInstance("a1");
        eng.instantiateSystem();
        System.out.println("==> Ok");

        System.out.println("\nRemoving instance when the instance doesn't exist : \n");
        /*
        try{
            eng.removeInstance("a1");
            eng.instantiateSystem();
        }catch(Exception e){
            System.out.println("Exception catched");
        }*/
        System.out.println("==> Ok");
    }

    public static void main(String argv[]) {
       //printerTest();

       testRelationModif();
       testInstanceModif();
       testEmptyAgg();
       testEmptyAgg2();
       testEmptyAgg3();
       testEmptyAgg4();
    }
}
