# jPRMengine wrapper

This project is used to generate a minimalistic wrapper of aGrUM (https://agrum.gitlab.io/) to be used with the prototype PRIME for probabilistic rules compilation and execution.

## How to use :

1) Install aGrUM in the parent folder
    1) `$> git clone https://gitlab.com/agrumery/aGrUM.git`
    2) `$> cd aGrUM`
    3) `$> git checkout -t origin/feature/noparent4agg` (*)
    4) `$> python act install release agrum`
2) Update compiler's info in the Makefile (GPP) as well as JDK's path (JAVA_INCLUDE)
3) Optionally, `$> make clean`
4) `$> make` (**)

(*) some work need to be done in order to be usable with master 
(**) sometimes an error occurs and is fixed by running the operation one more time
